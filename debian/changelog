xtpcpp (0.3.7-3) buster; urgency=medium

  * no console in windows version
  * pseudo centroid for tandem wrapper

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 20 Apr 2020 08:31:29 +0200

xtpcpp (0.3.7-2) buster; urgency=medium

  * important fix in lib PAPPSOms++

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sun, 12 Apr 2020 17:14:51 +0200

xtpcpp (0.3.7-1) buster; urgency=medium

  * allow column move (peptide list, protein list, ptm list, ptm peptide list)
  * edition of centroid parameters in tandem settings for tims tof reader
  * defining alignment groups
  * cross compilation with MXE

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 10 Apr 2020 22:05:08 +0200

xtpcpp (0.3.6-1) buster; urgency=medium

  * timstof support

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 04 Mar 2020 16:59:21 +0100

xtpcpp (0.3.5-1ubuntu1) bionic; urgency=medium

  * tandem error messages

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 10 Feb 2020 10:58:19 +0100

xtpcpp (0.3.4-1) buster; urgency=medium

  * several issues fixed by Thomas Renne
  * mascot dat parser work

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 29 Jan 2020 10:22:48 +0100

xtpcpp (0.3.3-1) buster; urgency=medium

  * new spreadsheet output for PSMs

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 14 Jan 2020 10:33:42 +0100

xtpcpp (0.3.2-2ubuntu1) bionic; urgency=medium

  * ubuntu 18.04 package 

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 10 Jan 2020 22:39:39 +0000

xtpcpp (0.3.2-2) buster; urgency=medium

  * new pappsoms++ library

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 08 Jan 2020 22:22:40 +0100

xtpcpp (0.3.2-1) buster; urgency=medium

  * better ms run identification view
  * prapagation of the project name

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 06 Jan 2020 11:26:14 +0100

xtpcpp (0.3.1-3) buster; urgency=medium

  * bunch of little bug fix

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 20 Dec 2019 14:21:34 +0100

xtpcpp (0.3.1-2) buster; urgency=medium

  * merge of latest changes from forge mia

 -- Olivier Langella <olivier.langella@u-psud.fr>  Thu, 19 Dec 2019 22:34:59 +0100

xtpcpp (0.3.1-1) buster; urgency=medium

  * new features coded by Thomas Renne
  * new msrun list windows
  * instant msrun statistics
  * many GUI  fix

 -- Olivier Langella <olivier.langella@u-psud.fr>  Thu, 19 Dec 2019 22:26:05 +0100

xtpcpp (0.2.40-2) buster; urgency=medium

  * buster package

 -- Olivier Langella <olivier.langella@u-psud.fr>  Thu, 12 Sep 2019 20:25:49 +0200

xtpcpp (0.2.40-1) stretch; urgency=medium

  * find spectrum directly by index if scan numbers are not supported
  * MassChroqML output without peptide labels in comments
  * new UI to choose removal contaminant mode
  * new FDR computation : decoy/target
  * FDR driven peptide theshold, available for Tandem and Mascot results

 -- Olivier Langella <olivier.langella@u-psud.fr>  Thu, 12 Sep 2019 09:10:45 +0200

xtpcpp (0.2.38-4) stretch; urgency=medium

  * scoring, pluggable scoring added

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 18 Jun 2019 13:37:52 +0200

xtpcpp (0.2.38-3) stretch; urgency=medium

  * refine, maximum missed cleavage sites added

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 18 Jun 2019 13:28:02 +0200

xtpcpp (0.2.38-2) stretch; urgency=medium

  * protein, use minimal annotation parameter added

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 18 Jun 2019 11:43:37 +0200

xtpcpp (0.2.38-1) stretch; urgency=medium

  * new button to look for the best MS run to use as reference in alignment
  * accepts to run TPP modified version of X!Tandem, using specific parameters
  (DIA requirements)
  * new PAPPSOms++ library

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 17 Jun 2019 13:53:16 +0200

xtpcpp (0.2.37-2) stretch; urgency=medium

  * sum/max extraction bug fixed, thanks to Marlène and Thierry

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 28 May 2019 16:06:10 +0200

xtpcpp (0.2.37-1) stretch; urgency=medium

  * new PAPPSOms++ library

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 24 May 2019 11:53:47 +0200

xtpcpp (0.2.36-1) stretch; urgency=medium

  * proticdbml fix
  * new tandem.exe setup control
  * better error message for tandem run process

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 16 Apr 2019 15:03:10 +0200

xtpcpp (0.2.35-2) stretch; urgency=medium

  * fix libodsstream problem

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 25 Mar 2019 11:27:19 +0100

xtpcpp (0.2.35-1) stretch; urgency=medium

  * mutation support
  * peprepro filter

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 25 Mar 2019 10:46:03 +0100

xtpcpp (0.2.34-2) stretch; urgency=medium

  * fix filename bug

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 19 Mar 2019 08:53:24 +0100

xtpcpp (0.2.34-1) stretch; urgency=medium

  * fix ppm range in MassChroqML export (thanks to Thierry Balliau)
  * new features asked by Plateforme Post-génomique de la Pitié Salpétrière - P3S 
  (Solenne Chardonnet, Cédric Pionneau)
  * write protein list view as is in ODS file
  * write peptide list view as is in ODS file
  * new ui menu in peptide and protein list wiew
  * ppm delta added in ODS spectra sheet export
  * display Nter and Cter peptide flanking regions in ODS and list views

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 18 Mar 2019 11:06:11 +0100

xtpcpp (0.2.33-1) stretch; urgency=medium

  * scroll area for small screens
  * xic extraction method (sum or max)
  * resilient ODS on failure
  * global settings windows
  * xic extractor reader choice

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 25 Feb 2019 13:29:57 +0100

xtpcpp (0.2.32-1) stretch; urgency=medium

  * moving to refactored PAPPSOms++ library
  * bug fixed in FASTA exports

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 06 Feb 2019 11:48:02 +0100

xtpcpp (0.2.31-2) stretch; urgency=medium

  * user agent created, variable to disable htpp get version

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 01 Feb 2019 14:36:56 +0100

xtpcpp (0.2.31-1) stretch; urgency=medium

  * new Fasta file exports by groups and subgroups
  * better MassChroqML output : check MS run file paths, set output result files, set alignment/xic exctraction/peak detection parameters

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 28 Jan 2019 15:13:20 +0100

xtpcpp (0.2.30-1) stretch; urgency=medium

  * new ODS spreadsheet dedicated to peptidomic : comparison of the
    number of spectra for each peptide in each sample

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 14 Jan 2019 10:43:00 +0100

xtpcpp (0.2.29-4) stretch; urgency=medium

  * new PAPPSOms++ library

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 08 Jan 2019 16:19:34 +0100

xtpcpp (0.2.29-3) stretch; urgency=medium

  * new pappsomspp library

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 02 Jan 2019 21:52:00 +0100

xtpcpp (0.2.29-1) stretch; urgency=medium

  * modification to be qcustomplot 2.0.1 compliant
  * accept labeling method containing an empty label

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sat, 15 Dec 2018 16:51:44 +0100

xtpcpp (0.2.28-3) stretch; urgency=medium

  * mascot takes all query candidates
  * fix thread wait loading spectrum

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 07 Dec 2018 10:44:46 +0100

xtpcpp (0.2.28-2) stretch; urgency=medium

  * take only first spectrum psm in mascot parser

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 27 Nov 2018 09:21:43 +0100

xtpcpp (0.2.28-1) stretch; urgency=medium

  * improved mascot dat parser
  * ability to choose TSV as export file format
  * icon added for Windows
  * more checks on tandem binary path
  * dependency added on package tandem-mass

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 26 Nov 2018 15:29:21 +0100

xtpcpp (0.2.27-1) stretch; urgency=medium

  * look for sequence LI

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 13 Nov 2018 14:58:53 +0100

xtpcpp (0.2.26-1) stretch; urgency=medium

  * display distinctively the FDR on peptides and on PSM

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 09 Nov 2018 10:50:11 +0100

xtpcpp (0.2.25-2) stretch; urgency=medium

  * new version number 0.2.25

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 02 Nov 2018 15:31:01 +0100

xtpcpp (0.2.25-1) stretch; urgency=medium

  * mascot parser fix for single fasta file
  * protein mass including alanine
  * X!Tandem version check
  * FDR written as percentage in ODS files
  * compute peptide FDR directly on peptides (no protein validation)

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 02 Nov 2018 14:26:19 +0100

xtpcpp (0.2.24-3) stretch; urgency=medium

  * fix for temporary directory under condor

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 29 Oct 2018 11:14:33 +0100

xtpcpp (0.2.24-2) stretch; urgency=medium

  * ODS output fix

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 19 Oct 2018 16:57:33 +0200

xtpcpp (0.2.24-1) stretch; urgency=medium

  * new compar ODS output dedicated to labeled experiments
  * SEVERE bug fix concerning protein Evalue computation (reported by T. Balliau) 

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 19 Oct 2018 16:13:10 +0200

xtpcpp (0.2.23-2) stretch; urgency=medium

  * fix version number

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 16 Oct 2018 11:13:36 +0200

xtpcpp (0.2.23-1) stretch; urgency=medium

  * checking tandem.exe installation
  * new controls to select decoy sequences and avoid bug signaled by C. Henry and T. Balliau
  * new temporary folder handler for HTCondor jobs 

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 16 Oct 2018 10:57:47 +0200

xtpcpp (0.2.22-3) stretch; urgency=medium

  * major mzIdentML improvements for MS-GF+

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 03 Oct 2018 22:32:56 +0200

xtpcpp (0.2.22-2) stretch; urgency=medium

  * minor fix

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 03 Oct 2018 11:08:30 +0200

xtpcpp (0.2.22-1) stretch; urgency=medium

  * better Mascot parser
  * new placeholder for contaminant widget
  * refresh evalue when choosing threshold
  * display contaminant checkbox in protein view
  * release MSrun after reading

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 02 Oct 2018 13:56:06 +0200

xtpcpp (0.2.21-4) stretch; urgency=medium

  * make sure that we look for MSrun in the right place

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 03 Sep 2018 16:29:47 +0200

xtpcpp (0.2.21-3) stretch; urgency=medium

  * smart file path guess

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 31 Aug 2018 11:49:39 +0200

xtpcpp (0.2.21-2) stretch; urgency=medium

  * gui text fix (proteins => peptides)
  * find readable msRun file

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 29 Aug 2018 09:26:21 +0200

xtpcpp (0.2.21-1) stretch; urgency=medium

  * ODS column consistency checked, more auto documentation

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 17 Aug 2018 13:51:19 +0200

xtpcpp (0.2.20-1) stretch; urgency=medium

  * TIC columns added in ODS report

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 13 Jul 2018 09:31:03 +0200

xtpcpp (0.2.19-1) stretch; urgency=medium

  * fix in mzidentml reader and pepxml reader to be able to load files
    from MSfragger

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 09 Jul 2018 13:36:40 +0200

xtpcpp (0.2.18-1) stretch; urgency=medium

  * pepxml support
  * XPIP change : each peptide is related has is own identification engine id
  * better xic extractor cache by msrun
  * possibility to display specific identification engine attributes (omssa, comet,msgf+...)

 -- Olivier Langella <olivier.langella@u-psud.fr>  Thu, 05 Jul 2018 13:50:31 +0200

xtpcpp (0.2.17-1) stretch; urgency=medium

  * new window to edit detection parameters
  * new PAPPSO library API to 
    read MS run files

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sat, 02 Jun 2018 18:09:53 +0200

xtpcpp (0.2.16-3) stretch; urgency=medium

  * fix masschroq out put msrun reference

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sat, 26 May 2018 07:05:21 +0200

xtpcpp (0.2.16-2) stretch; urgency=medium

  * falling back to simple pwiz xic extractor

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sat, 26 May 2018 06:47:15 +0200

xtpcpp (0.2.16-1) stretch; urgency=medium

  * new buffered XIC extractor

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 25 May 2018 09:08:47 +0200

xtpcpp (0.2.15-2) stretch; urgency=medium

  * lib pappsomspp change

 -- Olivier Langella <olivier.langella@u-psud.fr>  Thu, 17 May 2018 22:18:27 +0200

xtpcpp (0.2.15-1) stretch; urgency=medium

  * new xic extractor API, spectral count export fallback, compar bug
    fixed

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 16 May 2018 11:36:53 +0200

xtpcpp (0.2.14-2) stretch; urgency=medium

  * avoid libodsstream bug in compar sheets

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 14 May 2018 13:01:39 +0200

xtpcpp (0.2.14-1) stretch; urgency=medium

  * NSAF compar export

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 04 May 2018 09:14:35 +0200

xtpcpp (0.2.13-1) stretch; urgency=medium

  * XIC peak borders

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 16 Apr 2018 13:34:17 +0200

xtpcpp (0.2.12-2) stretch; urgency=medium

  * ppm delta bug fix

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 28 Mar 2018 10:55:01 +0200

xtpcpp (0.2.12-1) stretch; urgency=medium

  * GUI improvements

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 26 Mar 2018 16:14:42 +0200

xtpcpp (0.2.11-1) stretch; urgency=medium

  * MASCOT dat parser
  * contaminant widget

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 13 Mar 2018 16:01:10 +0100

xtpcpp (0.2.10-4) stretch; urgency=medium

  * bug fix when no peak has been detected for a peptide

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 14 Feb 2018 10:20:28 +0100

xtpcpp (0.2.10-3) stretch; urgency=medium

  * new PAPPSOms++ library

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 12 Feb 2018 16:45:57 +0100

xtpcpp (0.2.10-2) stretch; urgency=medium

  * fix library package name

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sun, 11 Feb 2018 18:08:26 +0100

xtpcpp (0.2.10-1) stretch; urgency=medium

  * better UI and XIC viewer

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sun, 11 Feb 2018 08:02:37 +0100

xtpcpp (0.2.9-2) stretch; urgency=medium

  * extract XIC in other MS runs

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 29 Jan 2018 09:53:12 +0100

xtpcpp (0.2.8-1) stretch; urgency=medium

  * better PTM island mode

 -- Olivier Langella <olivier.langella@u-psud.fr>  Thu, 25 Jan 2018 10:06:17 +0100

xtpcpp (0.2.7-1) stretch; urgency=medium

  * bug fixed on long protein label with reversed sequences

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 23 Jan 2018 15:57:47 +0100

xtpcpp (0.2.6-1) stretch; urgency=medium

  * new XIC viewer

 -- Olivier Langella <olivier.langella@u-psud.fr>  Thu, 18 Jan 2018 16:28:07 +0100

xtpcpp (0.2.5-2) stretch; urgency=medium

  * disable warning message in xpip export

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 10 Jan 2018 16:19:17 +0100

xtpcpp (0.2.5-1) stretch; urgency=medium

  * search on msrun and scan, better individual mode, better widgets

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 10 Jan 2018 14:50:52 +0100

xtpcpp (0.2.4-1) stretch; urgency=medium

  * new peptide widget

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 03 Jan 2018 11:44:36 +0100

xtpcpp (0.2.3-2) stretch; urgency=medium

  * new mass delta, new FDR, new histogram

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 20 Dec 2017 11:20:52 +0100

xtpcpp (0.2.3-1) stretch; urgency=medium

  * better FDR and mass delta computing

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sun, 17 Dec 2017 18:19:11 +0100

xtpcpp (0.2.2-2) stretch; urgency=medium

  * new pappsomspp library

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 15 Dec 2017 11:38:38 +0100

xtpcpp (0.2.2-1) stretch; urgency=medium

  * better xpip format input and output

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 04 Dec 2017 10:54:31 +0100

xtpcpp (0.2.1-1) stretch; urgency=medium

  * huge refactoring
  * new xpip output file
  * clear mz file list in tandem run dialog

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 22 Nov 2017 15:56:02 +0100

xtpcpp (0.1.23-1) stretch; urgency=medium

  * better ODS and XPIP file
  * compute mass and number of tryptic
    peptides on non regular protein sequences

 -- Olivier Langella <olivier.langella@u-psud.fr>  Wed, 15 Nov 2017 12:57:36 +0100

xtpcpp (0.1.22-2) stretch; urgency=medium

  * HTCondor parameter edition

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 06 Nov 2017 08:44:17 +0100

xtpcpp (0.1.22-1) stretch; urgency=medium

  * new buttons in central widget
  * xpip file creation
  * tandem parameters added
  * avoid non amino acid chars in tryptic peptide count

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 27 Oct 2017 16:40:50 +0200

xtpcpp (0.1.21-1) stretch; urgency=medium

  * tandem file check
  * better tandem form
  * multiple directory upload
  * monitoring grouping (and faster grouping)

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sat, 21 Oct 2017 11:58:51 +0200

xtpcpp (0.1.20-2) stretch; urgency=medium

  * tandem parameter form hardened

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sun, 15 Oct 2017 20:32:27 +0200

xtpcpp (0.1.20-1) stretch; urgency=medium

  * tandem parameters editor

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sun, 15 Oct 2017 07:50:50 +0200

xtpcpp (0.1.19-1) stretch; urgency=medium

  * new preset window (for tandem parameters)
  * divizion by zero fixed and exception added

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 09 Oct 2017 09:03:30 +0200

xtpcpp (0.1.18-1) stretch; urgency=medium

  * MS run and identification statistics

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 26 Sep 2017 10:07:10 +0200

xtpcpp (0.1.17-2) stretch; urgency=medium

  * fix desktop file

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 22 Sep 2017 15:42:11 +0200

xtpcpp (0.1.17-1) stretch; urgency=medium

  * better tandem run dialog box
  * success message after tandem run (thanks T. Balliau)
  * condor/tmp_dir_autoremove parameter to keep log files
  * choose number of threads for tandem run
  * XPIP mime type associated to xtpcpp

 -- Olivier Langella <olivier.langella@u-psud.fr>  Fri, 22 Sep 2017 14:36:15 +0200

xtpcpp (0.1.16-1) stretch; urgency=medium

  * qt5 switch
  * about dialog window
  * Tandem run window (local and HTCondor)

 -- Olivier Langella <olivier.langella@u-psud.fr>  Mon, 18 Sep 2017 12:07:33 +0200

xtpcpp (0.1.15-1) stretch; urgency=medium

  * ptm peptide list

 -- Olivier Langella <olivier.langella@u-psud.fr>  Thu, 17 Aug 2017 15:40:53 +0200

xtpcpp (0.1.14-2) stretch; urgency=medium

  * utf8 pwiz library bug workaround

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sat, 01 Jul 2017 10:37:11 +0200

xtpcpp (0.1.14-1) stretch; urgency=medium

  * mzidentml and pepxml support

 -- Olivier Langella <olivier.langella@u-psud.fr>  Sat, 24 Jun 2017 22:43:53 +0200

xtpcpp (0.1.13-1) jessie; urgency=medium

  * progress bar, now spectral count output

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Mon, 12 Jun 2017 08:59:00 +0200

xtpcpp (0.1.12-1) jessie; urgency=medium

  * first PTM island support

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Wed, 31 May 2017 10:51:11 +0200

xtpcpp (0.1.11-2) jessie; urgency=medium

  * new odsstream library

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Tue, 23 May 2017 23:09:30 +0200

xtpcpp (0.1.11-1) jessie; urgency=medium

  * proticdbml output

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Sat, 20 May 2017 17:36:09 +0200

xtpcpp (0.1.10-1) jessie; urgency=medium

  * trick to read MGF files

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Fri, 12 May 2017 10:26:42 +0200

xtpcpp (0.1.9-1) jessie; urgency=medium

  * MGF peak sorting, waiting dialog, new ODS sheets (samples, groups)

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Tue, 09 May 2017 11:30:23 +0200

xtpcpp (0.1.8-1) jessie; urgency=medium

  * better peptide viewer

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Fri, 05 May 2017 16:28:16 +0200

xtpcpp (0.1.7-1) jessie; urgency=medium

  * button to link with peptide viewer

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Thu, 04 May 2017 14:47:48 +0200

xtpcpp (0.1.6-1) jessie; urgency=medium

  * new ODS sheet in outputs

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Wed, 03 May 2017 10:28:25 +0200

xtpcpp (0.1.5-2) jessie; urgency=medium

  * new export options

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Fri, 28 Apr 2017 10:30:20 +0200

xtpcpp (0.1.5-1) jessie; urgency=medium

  * new export options

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Fri, 28 Apr 2017 10:18:20 +0200

xtpcpp (0.1.4-1) jessie; urgency=medium

  * FASTA export

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Thu, 27 Apr 2017 09:19:11 +0200

xtpcpp (0.1.3-1) jessie; urgency=medium

  * thanks Ariane and Thierry

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Tue, 25 Apr 2017 16:22:04 +0200

xtpcpp (0.1.2-1) jessie; urgency=medium

  * new modification editor, new FDR mass deviation behaviour, lots of
    improvements

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Mon, 24 Apr 2017 10:17:53 +0200

xtpcpp (0.1.1-1) jessie; urgency=medium

  * bug fix and new features

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Thu, 20 Apr 2017 11:32:40 +0200

xtpcpp (0.1.0-1) jessie; urgency=medium

  * first jessie package

 -- Olivier Langella <Olivier.Langella@moulon.inra.fr>  Wed, 19 Apr 2017 10:15:33 +0200

xtpcpp (0.1.0-1~stretch) stretch; urgency=medium

  * first package

 -- Olivier Langella <olivier.langella@u-psud.fr>  Tue, 18 Apr 2017 20:55:14 +0200

