Source: xtpcpp
Maintainer: Olivier Langella <olivier.langella@u-psud.fr>
Homepage: http://pappso.inra.fr/bioinfo
Section: science
Priority: optional
Build-Depends: debhelper (>= 9),
               cmake (>= 2.6),
               qtbase5-dev,
               libpappsomspp-widget-dev (>= @LIBPAPPSOMSPP_VERSION@),
               libpwiz-dev,
               libodsstream-qt5-dev,
               libqt5svg5-dev,
               libboost-dev,
               libqcustomplot-dev
Standards-Version: 3.9.8

Package: xtpcpp
Architecture: any
Multi-Arch: no
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libpappsomspp-widget0 (>= @LIBPAPPSOMSPP_VERSION@),
         tandem-mass,
         libpwiz3,
         libodsstream-qt5-0,
         libqt5gui5,
         libqt5core5a,
         libqt5xml5,
         libqt5svg5,
         libqt5network5,
         libqcustomplot2.0
Pre-Depends: ${misc:Pre-Depends}
Description: C++ version of X!TandemPipeline
