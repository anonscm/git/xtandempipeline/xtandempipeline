/**
 * \file utils/workmonitor.cpp
 * \date 10/6/2017
 * \author Olivier Langella
 * \brief monitoring progress in any worker thread
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "workmonitor.h"
#include <QDebug>

WorkMonitor::WorkMonitor()
{
  _max_value      = 100;
  _timer_duration = 200;
  _time.restart();
}

void
WorkMonitor::finished(const QString &message)
{
  emit workerJobFinished(message);
}

void
WorkMonitor::canceled(const QString &message)
{
  emit workerJobCanceled(message);
}

void
WorkMonitor::message(const QString &message)
{
  if(_time.elapsed() > _timer_duration)
    {
      _time.restart();
      emit workerMessage(message);
    }
}
void
WorkMonitor::message(const QString &message, int value)
{
  qDebug() << "WorkMonitor::message " << value << " " << _max_value << " "
           << (value / _max_value) * 100;
  if(_time.elapsed() > _timer_duration)
    {
      int percent = ((float)value / (float)_max_value) * (float)100;

      _time.restart();
      emit workerMessagePercent(message, percent);
    }
}
void
WorkMonitor::setProgressMaximumValue(int max_value)
{
  // qDebug() << "WorkMonitor::setProgressMaximumValue " << max_value;
  _max_value = max_value;
}

void
WorkMonitor::appendText(const char *p_char)
{
  // qDebug() << "WorkMonitor::setProgressMaximumValue " << max_value;
  emit workerAppendText(p_char);
}

void
WorkMonitor::setText(const QString text)
{
  emit workerSetText(text);
}
