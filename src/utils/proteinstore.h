/**
 * \file utils/proteinstore.h
 * \date 23/3/2017
 * \author Olivier Langella
 * \brief store unique version of proteins
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#ifndef PROTEINSTORE_H
#define PROTEINSTORE_H

#include "../core/proteinxtp.h"
#include "types.h"
#include <QString>
#include <QRegExp>
#include <map>
#include <vector>

class FastaFile;

class ProteinStore
{
  public:
  ProteinStore();
  ~ProteinStore();

  ProteinXtpSp &getInstance(ProteinXtpSp &protein_in);

  void setRegexpDecoyPattern(const QString &pattern);

  QRegExp getRegexpDecoy() const;

  void setRegexpContaminantPattern(const QString &pattern);

  QRegExp getRegexpContaminant() const;

  void addContaminantFastaFile(const FastaFile *p_fasta_file);
  void addDecoyFastaFile(const FastaFile *p_fasta_file);

  const std::vector<const FastaFile *> &getContaminantFastaFileList() const;
  const std::vector<const FastaFile *> &getDecoyFastaFileList() const;


  void clearContaminants();
  void clearDecoys();

  void setContaminantAccession(QString accession);
  void setDecoyAccession(QString accession);
  std::size_t size() const;

  const std::map<QString, ProteinXtpSp> &getProteinMap() const;

  private:
  void setProteinInformations(ProteinXtpSp &protein_in);

  private:
  std::map<QString, ProteinXtpSp> _map_accession_protein_list;

  /** \brief decoy Fasta file liste */
  std::vector<const FastaFile *> _fasta_decoy_list;
  /** \brief contaminant Fasta file liste */
  std::vector<const FastaFile *> _fasta_contaminant_list;

  /** \brief recognize decoy accession */
  QRegExp _regexp_decoy;
  /** \brief recognize contaminant accession */
  QRegExp _regexp_contaminant;
};

#endif // PROTEINSTORE_H
