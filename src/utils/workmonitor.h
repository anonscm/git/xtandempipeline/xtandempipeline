/**
 * \file utils/workmonitor.h
 * \date 10/6/2017
 * \author Olivier Langella
 * \brief monitoring progress in any worker thread
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QObject>
#include <QElapsedTimer>

class WorkMonitorInterface
{
  public:
  virtual void finished(const QString &message)           = 0;
  virtual void canceled(const QString &message)           = 0;
  virtual void message(const QString &message)            = 0;
  virtual void message(const QString &message, int value) = 0;
  virtual void setProgressMaximumValue(int max_value)     = 0;
  virtual void appendText(const char *p_char)             = 0;
  virtual void setText(const QString text)                = 0;
};

class WorkMonitor : public QObject, public WorkMonitorInterface
{
  Q_OBJECT
  public:
  WorkMonitor();
  void finished(const QString &message) override;
  void canceled(const QString &message) override;
  void message(const QString &message) override;
  void message(const QString &message, int value) override;
  void setProgressMaximumValue(int max_value) override;
  void appendText(const char *p_char) override;
  void setText(const QString text) override;

  signals:
  void workerJobFinished(QString message);
  void workerJobCanceled(QString message);
  void workerMessage(QString message);
  void workerMessagePercent(QString message, int value);
  void workerAppendText(const char *p_char);
  void workerSetText(QString text);

  private:
  QElapsedTimer _time;
  int _max_value;
  int _timer_duration;
};
