/**
 * \file grouping/ptm/ptmgroupingexperiment.h
 * \date 24/5/2017
 * \author Olivier Langella
 * \brief handle grouping experiment based on ptm islands
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef PTMGROUPINGEXPERIMENT_H
#define PTMGROUPINGEXPERIMENT_H

#include "../../core/proteinmatch.h"
#include <pappsomspp/amino_acid/aamodification.h>
#include "ptmisland.h"
#include "ptmislandsubgroup.h"
#include "ptmislandgroup.h"
class PtmSampleScan;

struct ModificationAndAa
{
  pappso::AaModificationP modification;
  std::vector<char> aa_list;
};

class PtmGroupingExperiment
{
  friend PtmSampleScan;

  public:
  PtmGroupingExperiment();
  ~PtmGroupingExperiment();

  /** @brief choose validation state of peptide to use in PTM grouping
   * experiment
   */
  void setValidationState(ValidationState validation_state);
  void addProteinMatch(const ProteinMatch *p_protein_match);
  void startGrouping();

  /** @brief get the ptm island subgroup list
   */
  const std::vector<PtmIslandSubgroupSp> &getPtmIslandSubgroupList() const;

  /** @brief get the ptm island group list
   */
  const std::vector<PtmIslandGroupSp> &getPtmIslandGroupList() const;

  /** @brief get the ptm island list
   */
  const std::vector<PtmIslandSp> &getPtmIslandList() const;

  /** @brief count number of modifications in a PeptideMatch
   * */
  unsigned int countPeptideMatchPtm(const PeptideMatch &peptide_match) const;
  std::vector<unsigned int>
  getPtmPositions(const PeptideMatch &peptide_match) const;

  private:
  std::vector<unsigned int>
  getPtmPositions(const ProteinMatch *protein_match) const;

  std::vector<PtmIslandSp>
  mergePeptideMatchPtmIslandList(std::vector<PtmIslandSp> ptm_island_list);
  void addPtmIsland(PtmIslandSp ptm_island);
  void numbering();

  private:
  std::list<ModificationAndAa> _modification_list;
  ValidationState _peptide_validation_state = ValidationState::validAndChecked;
  std::vector<PtmIslandSp> _ptm_island_list;
  std::vector<PtmIslandSubgroupSp> _ptm_island_subgroup_list;
  std::vector<PtmIslandGroupSp> _ptm_island_group_list;
};

#endif // PTMGROUPINGEXPERIMENT_H
