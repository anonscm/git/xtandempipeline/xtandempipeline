/**
 * \file /core/tandem_run/tandemrunbatch.h
 * \date 2/9/2017
 * \author Olivier Langella
 * \brief all data needed to run a Tandem batch
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QStringList>
#include <QMetaType>
struct TandemRunBatch
{
  QString _tandem_bin_path;
  QStringList _mz_file_list;
  QString _preset_file;
  QStringList _fasta_file_list;
  QString _output_directory;
  unsigned int _number_of_threads = 1;
};
// Q_DECLARE_METATYPE(TandemRunBatch)
