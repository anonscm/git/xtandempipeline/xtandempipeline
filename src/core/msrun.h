/**
 * \filed core/msrun.h
 * \date 5/4/2017
 * \author Olivier Langella
 * \brief describes an MS run (chemical sample injected in a mass spectrometer)
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once

#include <memory>
#include <QString>
#include <QVariant>
#include <pappsomspp/msrun/msrunid.h>
#include <pappsomspp/msrun/msrunreader.h>
#include <pappsomspp/msrun/alignment/msrunretentiontime.h>
#include <pappsomspp/xicextractor/msrunxicextractorinterface.h>
#include <pappsomspp/peptide/peptide.h>
#include "../utils/types.h"
#include "../utils/peptideevidencestore.h"
#include <utils/msrunstatisticshandler.h>

class MsRunAlignmentGroup;
typedef std::shared_ptr<MsRunAlignmentGroup> MsRunAlignmentGroupSp;

class MsRun;
typedef std::shared_ptr<MsRun> MsRunSp;

class MsRun : public pappso::MsRunId
{
  public:
  MsRun(const QString &location);
  MsRun(const MsRun &other);
  ~MsRun();


  /** \brief set MS run statistics
   * any statistics on this MS run file
   */
  virtual void setMsRunStatistics(MsRunStatistics param, const QVariant &value);


  /** \brief get MS run statistics
   */
  virtual const QVariant getMsRunStatistics(MsRunStatistics param) const;

  /** \brief get MS run statistics map
   */
  virtual const std::map<MsRunStatistics, QVariant> &
  getMsRunStatisticsMap() const;

  bool findMsRunFile();

  pappso::MsRunReaderSPtr &getMsRunReaderSPtr();

  /** @brief release shared pointer on MSrun reader
   */
  void freeMsRunReaderSp();

  void checkMsRunStatistics();
  void checkMsRunStatistics(MsRunStatisticsHandler *currentHandler);
  void checkMsRunStatisticsForTdf(MsRunStatisticsHandler *currentHandler);

  pappso::MsRunXicExtractorInterfaceSp getMsRunXicExtractorInterfaceSp();

  void
  buildMsRunRetentionTime(const PeptideEvidenceStore &peptide_evidence_store);
  void computeMsRunRetentionTime();
  void clearMsRunRetentionTime();
  pappso::MsRunRetentionTime<const pappso::Peptide *> *
  getMsRunRetentionTimePtr();
  void setTimstofMs2CentroidParameters(const QString &centroid_parameters);
  MsRunAlignmentGroupSp getAlignmentGroup();
  void setAlignmentGroup(MsRunAlignmentGroupSp new_group);

  private:
  std::map<MsRunStatistics, QVariant> _param_stats;

  pappso::MsRunReaderSPtr _msrun_reader_sp;

  pappso::MsRunXicExtractorInterfaceSp _xic_extractor_sp;

  pappso::MsRunRetentionTime<const pappso::Peptide *> *mpa_msrunRetentionTime =
    nullptr;

  QString m_centroidOptions;
  MsRunAlignmentGroupSp msp_alignmentGroup;
};


Q_DECLARE_METATYPE(MsRunSp)
