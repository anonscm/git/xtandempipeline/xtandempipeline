
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#pragma once

#include <memory>
#include "identificationgroup.h"
#include "automaticfilterparameters.h"
#include "../utils/types.h"
#include "../utils/peptidestore.h"
#include "../utils/proteinstore.h"
#include "../utils/identificationdatasourcestore.h"
#include "../utils/msrunstore.h"
#include "../utils/workmonitor.h"
#include "labeling/labelingmethod.h"
#include "alignmentgroup.h"

class Project;
typedef std::shared_ptr<Project> ProjectSp;

class PeptideMatch;
class ProteinMatch;

class Project
{
  public:
  Project();
  ~Project();

  ProjectSp makeProjectSp() const;
  ProteinStore &getProteinStore();
  PeptideStore &getPeptideStore();
  const ProteinStore &getProteinStore() const;
  MsRunStore &getMsRunStore();
  const MsRunStore &getMsRunStore() const;
  FastaFileStore &getFastaFileStore();
  const FastaFileStore &getFastaFileStore() const;
  IdentificationDataSourceStore &getIdentificationDataSourceStore();
  const IdentificationDataSourceStore &getIdentificationDataSourceStore() const;
  void readXpipFile(WorkMonitorInterface *p_monitor, QFileInfo xpip_source);
  IdentificationGroup *newIdentificationGroup();

  /** @brief validate or invalidate peptides and proteins based automatic
   * filters and manual checks
   * */
  void updateAutomaticFilters(
    const AutomaticFilterParameters &automatic_filter_parameters);
  const AutomaticFilterParameters &getAutomaticFilterParameters() const;
  void startGrouping(WorkMonitorInterface *p_work_monitor);

  GroupingType getGroupingType() const;
  ContaminantRemovalMode getContaminantRemovalMode() const;
  void
  setContaminantRemovalMode(ContaminantRemovalMode contaminant_removal_mode);
  ProjectMode getProjectMode() const;
  void setProjectMode(ProjectMode mode);
  void setProjectName(const QString &name);
  QString getProjectName();

  std::vector<MsRunAlignmentGroupSp> getMsRunAlignmentGroupList();
  MsRunAlignmentGroupSp getMsRunAlignmentGroupFromList(QString group_name);
  void addMsRunAlignmentGroupToList(MsRunAlignmentGroupSp new_group);

  std::vector<IdentificationGroup *> getIdentificationGroupList();
  const std::vector<IdentificationGroup *> getIdentificationGroupList() const;
  void readResultFile(QString filename);

  /** @brief check that modifications are coded with PSI MOD accessions
   */
  bool checkPsimodCompliance() const;

  /** @brief apply labeling method to all peptide match
   * */
  void setLabelingMethodSp(LabelingMethodSp labeling_method_sp);

  /** @brief get labeling method shared pointer
   * */
  LabelingMethodSp getLabelingMethodSp() const;

  bool hasPtmExperiment() const;

  /** @brief look for a peptide in the same XIC
   * @param peptide_evidence_list the peptide evidence list to build
   * @param p_msrun MSrun to look for
   * @param p_peptide peptide to look for
   * @param charge charge to look for
   */
  void getSameXicPeptideEvidenceList(
    std::vector<const PeptideEvidence *> &peptide_evidence_list,
    const MsRun *p_msrun,
    const PeptideXtp *p_peptide,
    unsigned int charge) const;


  void prepareMsrunRetentionTimesForAlignment() const;

  void setProjectChangedStatus();
  bool getProjectChangedStatus() const;

  private:
  LabelingMethodSp _labeling_method_sp;
  ProjectMode _project_mode = ProjectMode::combined;
  std::vector<IdentificationGroup *> _identification_goup_list;
  std::vector<IdentificationDataSourceSp> m_identificationDataSourceSpList;
  std::vector<MsRunAlignmentGroupSp> m_msRunAlignmentGroupList;
  IdentificationGroup *_p_current_identification_group = nullptr;

  AutomaticFilterParameters _automatic_filter_parameters;

  GroupingType m_groupingType = GroupingType::PeptideMass;
  ContaminantRemovalMode m_contaminantRemovalMode =
    ContaminantRemovalMode::groups;

  QString m_project_name = "untitled";
  ProteinStore _protein_store;
  PeptideStore _peptide_store;
  IdentificationDataSourceStore _identification_data_source_store;
  MsRunStore _msrun_store;
  FastaFileStore _fasta_file_store;
  bool m_project_changed = false;
};
