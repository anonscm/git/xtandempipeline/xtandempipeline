
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#include "identificationxtandemfile.h"
#include <pappsomspp/pappsoexception.h>
#include "../project.h"
#include "../../input/xtandemsaxhandler.h"

IdentificationXtandemFile::IdentificationXtandemFile(
  const QFileInfo &xtandem_file)
  : IdentificationDataSource(xtandem_file.absoluteFilePath()),
    _xtandem_file(xtandem_file)
{
  _engine = IdentificationEngine::XTandem;
}

IdentificationXtandemFile::IdentificationXtandemFile(
  const IdentificationXtandemFile &other)
  : IdentificationDataSource(other), _xtandem_file(other._xtandem_file)
{
  _engine = IdentificationEngine::XTandem;
}

IdentificationXtandemFile::~IdentificationXtandemFile()
{
}

pappso::MassSpectrumCstSPtr
IdentificationXtandemFile::getMassSpectrumCstSPtr(
  unsigned int scan_number) const
{
  pappso::MassSpectrumCstSPtr spectrum_sp =
    IdentificationDataSource::getMassSpectrumCstSPtr(scan_number);
  return spectrum_sp;
}


void
IdentificationXtandemFile::parseTo(Project *p_project)
{
  qDebug() << "IdentificationXtandemFile::parseTo begin";

  qDebug() << "Read X!Tandem XML result file '"
           << _xtandem_file.absoluteFilePath() << "'";

  MsRunSp msrun_sp =
    p_project->getMsRunStore().getInstance(QFileInfo(_xtandem_file).baseName());
  setMsRunSp(msrun_sp);
  std::vector<IdentificationGroup *> identification_list =
    p_project->getIdentificationGroupList();
  IdentificationGroup *identification_group_p = nullptr;
  if(p_project->getProjectMode() == ProjectMode::combined)
    {
      if(identification_list.size() == 0)
        {
          identification_group_p = p_project->newIdentificationGroup();
        }
      else
        {
          identification_group_p = identification_list[0];
        }
    }
  else
    {
      for(IdentificationGroup *identification_p_flist : identification_list)
        {
          if(identification_p_flist->containSample(
               msrun_sp.get()->getSampleName()))
            {
              identification_group_p = identification_p_flist;
              break;
            }
        }
      if(identification_group_p == nullptr)
        {
          identification_group_p = p_project->newIdentificationGroup();
        }
    }

  identification_group_p->addIdentificationDataSourceP(this);
  XtandemSaxHandler *parser =
    new XtandemSaxHandler(p_project, identification_group_p, this);

  QXmlSimpleReader simplereader;
  simplereader.setContentHandler(parser);
  simplereader.setErrorHandler(parser);

  QFile qfile(_xtandem_file.absoluteFilePath());
  QXmlInputSource xmlInputSource(&qfile);

  if(simplereader.parse(xmlInputSource))
    {

      qfile.close();
    }
  else
    {
      qDebug() << parser->errorString();
      // throw PappsoException(
      //    QObject::tr("error reading tandem XML result file :\n").append(
      //         parser->errorString()));

      qfile.close();

      throw pappso::PappsoException(
        QObject::tr("Error reading %1 XPIP file :\n %2")
          .arg(_xtandem_file.absoluteFilePath())
          .arg(parser->errorString()));
    }

  qDebug() << "IdentificationXtandemFile::parseTo end";
}

/*
MsRunSp
IdentificationXtandemFile::getMsRunSp() const
{
  if(_ms_run_sp.get() != nullptr)
    {
      _ms_run_sp.get()->setTimstofMs2CentroidParameters(m_centroidOptions);
    }
  return (_ms_run_sp);
}
*/

void
IdentificationXtandemFile::setTimstofMs2CentroidParameters(
  const QString &centroid_parameters)
{
  qDebug() << centroid_parameters;
  m_centroidOptions = centroid_parameters;
  /*
    double resolution,              //!< instrument resolution
    double smoothwidth        = 2., //!< smoothwidth
    double integrationWidth   = 4,  //! integration width
    double intensityThreshold = 10.,
    */

  if(_ms_run_sp.get() != nullptr)
    {
      _ms_run_sp.get()->setTimstofMs2CentroidParameters(m_centroidOptions);
    }
}
