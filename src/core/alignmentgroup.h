/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/
#pragma once

#include <QDebug>
#include <memory>
#include "msrun.h"
#include "project.h"

class MsRunAlignmentGroup;
typedef std::shared_ptr<MsRunAlignmentGroup> MsRunAlignmentGroupSp;

class MsRunAlignmentGroup
{
  public:
  MsRunAlignmentGroup(Project *project, QString group_name);
  ~MsRunAlignmentGroup();

  void setMsRunAlignmentGroupName(QString group_name);
  QString getMsRunAlignmentGroupName();

  void setMsRunReference(MsRunSp reference);
  MsRunSp getMsRunReference();

  std::list<MsRunSp> getMsRunsInAlignmentGroup() const;

  void addMsRunToMsRunAlignmentGroupList(MsRunSp new_msrun);
  void removeMsRunFromMsRunAlignmentGroupList(MsRunSp removed_msrun);
  void prepareMsrunRetentionTimesForAlignment() const;

  private:
  QString m_groupName;
  Project *mp_project;
  std::list<MsRunSp> m_msrunAlignmentGroupList;
  MsRunSp msp_msRunReference = nullptr;
};
