
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "fastafile.h"
#include <pappsomspp/fasta/fastareader.h>
#include <QStringList>
#include <QDebug>
#include <pappsomspp/exception/exceptionnotfound.h>

class AccessionContaminantReader : public pappso::FastaHandlerInterface
{
  public:
  AccessionContaminantReader(ProteinStore &protein_store)
    : _protein_store(protein_store){};

  void
  setSequence(const QString &description, const QString &sequence) override
  {
    // qDebug() << "PeptideReader::setSequence " << description << " "
    // <<sequence;
    QStringList descr_split = description.simplified().split(" ");
    QString accession       = descr_split.at(0);
    // qDebug() << "PeptideReader::setSequence " << accession << " "
    // <<accession;
    _protein_store.setContaminantAccession(accession);
  };

  private:
  ProteinStore &_protein_store;
};


class AccessionDecoyReader : public pappso::FastaHandlerInterface
{
  public:
  AccessionDecoyReader(ProteinStore &protein_store)
    : _protein_store(protein_store){};

  void
  setSequence(const QString &description, const QString &sequence) override
  {
    // qDebug() << "PeptideReader::setSequence " << description << " "
    // <<sequence;
    QStringList descr_split = description.simplified().split(" ");
    QString accession       = descr_split.at(0);
    _protein_store.setDecoyAccession(accession);
  };

  private:
  ProteinStore &_protein_store;
};


FastaFile::FastaFile(const QString &fasta_source) : _fasta_source(fasta_source)
{
  qDebug() << "FastaFile::FastaFile " << fasta_source;
}
FastaFile::FastaFile(const QFileInfo &fasta_source)
  : _fasta_source(fasta_source)
{
}
FastaFile::FastaFile(const FastaFile &other)
  : _fasta_source(other._fasta_source)
{
}
FastaFile::~FastaFile()
{
}

void
FastaFile::setXmlId(const QString xmlid)
{
  _xml_id = xmlid;
}
const QString &
FastaFile::getXmlId() const
{
  return _xml_id;
}
const QString
FastaFile::getFilename() const
{
  return _fasta_source.fileName();
}

const QString
FastaFile::getAbsoluteFilePath() const
{
  return _fasta_source.absoluteFilePath();
}
void
FastaFile::setContaminants(ProteinStore &protein_store) const
{
  protein_store.addContaminantFastaFile(this);
  if(_fasta_source.exists())
    {
      AccessionContaminantReader accession_reader(protein_store);
      pappso::FastaReader reader(accession_reader);
      QFile fasta_file(_fasta_source.absoluteFilePath());
      if(fasta_file.open(QIODevice::ReadOnly))
        {
          reader.parse(&fasta_file);
          fasta_file.close();
        }
      else
        {
          qDebug() << "FastaFile::setContaminants "
                   << _fasta_source.absoluteFilePath() << " not open";
          throw pappso::ExceptionNotFound(
            QObject::tr("unable to open contaminant fasta file \"%1\"")
              .arg(_fasta_source.absoluteFilePath()));
        }
    }
  else
    {
      qDebug() << "FastaFile::setContaminants "
               << _fasta_source.absoluteFilePath() << " does not exists";
      throw pappso::ExceptionNotFound(
        QObject::tr("contaminant fasta file \"%1\" not found")
          .arg(_fasta_source.absoluteFilePath()));
    }
}

void
FastaFile::setDecoys(ProteinStore &protein_store) const
{
  protein_store.addDecoyFastaFile(this);
  if(_fasta_source.exists())
    {
      AccessionDecoyReader accession_reader(protein_store);
      pappso::FastaReader reader(accession_reader);
      QFile fasta_file(_fasta_source.absoluteFilePath());
      if(fasta_file.open(QIODevice::ReadOnly))
        {
          reader.parse(&fasta_file);
          fasta_file.close();
        }
      else
        {
          qDebug() << "FastaFile::setDecoys "
                   << _fasta_source.absoluteFilePath() << " not open";
          throw pappso::ExceptionNotFound(
            QObject::tr("unable to open decoy fasta file \"%1\"")
              .arg(_fasta_source.absoluteFilePath()));
        }
    }
  else
    {
      qDebug() << "FastaFile::setDecoys " << _fasta_source.absoluteFilePath()
               << " does not exists";
      throw pappso::ExceptionNotFound(
        QObject::tr("decoy fasta file \"%1\" not found")
          .arg(_fasta_source.absoluteFilePath()));
    }
}
