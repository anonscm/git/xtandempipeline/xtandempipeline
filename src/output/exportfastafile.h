/**
 * \file output/exportfastafile.h
 * \date 15/01/2019
 * \author Olivier Langella
 * \brief FASTA file writer
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QString>
#include "../utils/types.h"
#include "../core/project.h"
#include <pappsomspp/fasta/fastaoutputstream.h>

class ExportFastaFile
{
  private:
  QFile *mp_outputFastaFile = nullptr;
  ExportFastaType m_exportType;

  private:
  void writeIdentificationGroup(pappso::FastaOutputStream &fasta_output,
                                IdentificationGroup *p_ident);

  public:
  ExportFastaFile(QString filename, ExportFastaType type);
  virtual ~ExportFastaFile();
  void write(ProjectSp project_sp);
  void close();
};
