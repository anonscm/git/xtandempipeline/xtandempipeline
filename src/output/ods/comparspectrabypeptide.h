/**
 * \file output/ods/comparspectrabypeptide.h
 * \date 11/01/2018
 * \author Olivier Langella
 * \brief usefull for peptidomic
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#pragma once

#include "../../core/project.h"
#include <odsstream/calcwriterinterface.h>
#include "../../core/proteinmatch.h"
#include "odsexport.h"

class ComparSpectraByPeptide
{

  private:
  OdsExport *_p_ods_export;
  const Project *_p_project;
  CalcWriterInterface *_p_writer;
  LabelingMethodSp _sp_labelling_method;
  std::vector<Label *> _label_list;
  std::vector<MsRunSp> _msrun_list;
  IdentificationGroup *_p_current_identification_group;
  QString _first_cell_coordinate;

  private:
  void writeIdentificationGroup(IdentificationGroup *p_ident);
  void writeHeaders(IdentificationGroup *p_ident);
  void writeBestPeptideEvidence(
    const GroupingGroup *p_group,
    const PeptideEvidence *p_peptide_evidence,
    const std::map<QString, std::vector<size_t>> &sample_scan_list);
  void writeComparValue(
    const std::map<QString, std::vector<size_t>> &sample_scan_list,
    ValidationState state,
    const MsRun *p_msrun,
    const Label *p_label = nullptr);

  public:
  ComparSpectraByPeptide(OdsExport *p_ods_export,
                         CalcWriterInterface *p_writer,
                         const Project *p_project);
  void writeSheet();
};
