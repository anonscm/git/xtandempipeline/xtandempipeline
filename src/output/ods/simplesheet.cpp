/**
 * \file output/ods/simplesheet.h
 * \date 11/4/2017
 * \author Olivier Langella
 * \brief ODS simple sheet
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "simplesheet.h"
#include "../../core/identificationgroup.h"

SimpleSheet::SimpleSheet(CalcWriterInterface *p_writer,
                         const Project *p_project)
  : _p_project(p_project)
{
  _p_writer = p_writer;
  p_writer->writeSheet("simple");

  std::vector<IdentificationGroup *> identification_list =
    p_project->getIdentificationGroupList();

  for(IdentificationGroup *p_ident : identification_list)
    {
      writeIdentificationGroup(p_ident);
    }
}

void
SimpleSheet::writeIdentificationGroup(IdentificationGroup *p_ident)
{

  //_p_writer->writeSheet("simple");
  const std::vector<MsRunSp> msrun_list = p_ident->getMsRunSpList();
  _p_writer->writeCell("protein");
  _p_writer->writeLine();
  _p_writer->writeEmptyCell();
  _p_writer->writeCell("peptide");
  _p_writer->writeLine();
  if(msrun_list.size() == 1)
    {
      _p_writer->writeCell("sample");
      _p_writer->writeLine();
      _p_writer->writeCell(msrun_list[0].get()->getSampleName());
      _p_writer->writeLine();
    }

  for(ProteinMatch *p_protein_match : p_ident->getProteinMatchList())
    {
      _p_writer->writeCell(
        p_protein_match->getProteinXtpSp().get()->getAccession());
      _p_writer->writeLine();
    }
}
