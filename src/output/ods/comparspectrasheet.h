/**
 * \file output/ods/comparspectrasheet.h
 * \date 30/4/2017
 * \author Olivier Langella
 * \brief ODS compar spectra sheet
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef COMPARSPECTRASHEET_H
#define COMPARSPECTRASHEET_H

#include "comparbasesheet.h"

class ComparSpectraSheet : public ComparBaseSheet
{
  public:
  ComparSpectraSheet(OdsExport *p_ods_export,
                     CalcWriterInterface *p_writer,
                     const Project *p_project);

  protected:
  virtual void writeComparValue(const ProteinMatch *p_protein_match,
                                ValidationState state,
                                const MsRun *p_msrun,
                                const Label *p_label = nullptr) override;
};


class ComparSequenceSheet : public ComparBaseSheet
{
  public:
  ComparSequenceSheet(OdsExport *p_ods_export,
                      CalcWriterInterface *p_writer,
                      const Project *p_project);

  protected:
  virtual void writeComparValue(const ProteinMatch *p_protein_match,
                                ValidationState state,
                                const MsRun *p_msrun,
                                const Label *p_label = nullptr) override;
};

class ComparPaiSheet : public ComparBaseSheet
{
  public:
  ComparPaiSheet(OdsExport *p_ods_export,
                 CalcWriterInterface *p_writer,
                 const Project *p_project);

  protected:
  virtual void writeComparValue(const ProteinMatch *p_protein_match,
                                ValidationState state,
                                const MsRun *p_msrun,
                                const Label *p_label = nullptr) override;
};

class ComparEmpaiSheet : public ComparBaseSheet
{
  public:
  ComparEmpaiSheet(OdsExport *p_ods_export,
                   CalcWriterInterface *p_writer,
                   const Project *p_project);

  protected:
  virtual void writeComparValue(const ProteinMatch *p_protein_match,
                                ValidationState state,
                                const MsRun *p_msrun,
                                const Label *p_label = nullptr) override;
};

class ComparNsafSheet : public ComparBaseSheet
{
  public:
  ComparNsafSheet(OdsExport *p_ods_export,
                  CalcWriterInterface *p_writer,
                  const Project *p_project);

  protected:
  virtual void writeComparValue(const ProteinMatch *p_protein_match,
                                ValidationState state,
                                const MsRun *p_msrun,
                                const Label *p_label = nullptr) override;

  private:
  std::map<QString, pappso::pappso_double> _map_proto_nsaf_sum_by_msrun;
};

#endif // COMPARSPECTRASHEET_H
