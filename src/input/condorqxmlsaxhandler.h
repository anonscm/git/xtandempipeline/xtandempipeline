/**
 * \file input/condorqxmlsaxhandler.h
 * \date 15/9/2017
 * \author Olivier Langella
 * \brief parse condor_q XML
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef CONDORQXMLSAXHANDLER_H
#define CONDORQXMLSAXHANDLER_H

#include <QXmlDefaultHandler>

class TandemCondorProcess;

/** \def CondorJobStatus
 * */
enum class CondorJobStatus : std::int8_t
{
  Unexpanded     = 0, ///< 0 Unexpanded U
  Idle           = 1, ///< 1 Idle I
  Running        = 2, ///< 2 Running R
  Removed        = 3, ///< 3 Removed X
  Completed      = 4, ///< 4 Completed C
  Held           = 5, ///< 5 Held H
  Submission_err = 6  ///< 6 Submission_err E
};

class CondorQxmlSaxHandler : public QXmlDefaultHandler
{
  public:
  CondorQxmlSaxHandler(TandemCondorProcess *tandem_condor_process);
  ~CondorQxmlSaxHandler();

  bool startElement(const QString &namespaceURI,
                    const QString &localName,
                    const QString &qName,
                    const QXmlAttributes &attributes);

  bool endElement(const QString &namespaceURI,
                  const QString &localName,
                  const QString &qName);

  bool startDocument();

  bool endDocument();

  bool characters(const QString &str);

  bool fatalError(const QXmlParseException &exception);
  bool error(const QXmlParseException &exception);

  QString errorString() const;

  private:
  bool startElement_c(QXmlAttributes attributes);
  bool startElement_a(QXmlAttributes attributes);
  bool endElement_c();
  bool endElement_a();
  bool endElement_i();
  bool endElement_s();

  private:
  TandemCondorProcess *_tandem_condor_process;

  std::vector<QString> _tag_stack;
  QString _errorStr;
  QString _current_text;

  QString _in_name;
  CondorJobStatus _condor_job_status;
  unsigned int _current_proc_id;
  QString _current_remote_host;
  QString _current_last_remote_host;

  std::int8_t _count_status[10];
  bool _is_empty;
};

#endif // CONDORQXMLSAXHANDLER_H
