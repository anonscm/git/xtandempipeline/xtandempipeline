/**
 * \file /input/xtandemparamsaxhandler.h
 * \date 19/9/2017
 * \author Olivier Langella
 * \brief XML handler for X!Tandem parameters file (presets)
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QXmlDefaultHandler>
#include <vector>
#include "../core/tandem_run/tandemparameters.h"

class XtandemParamSaxHandler : public QXmlDefaultHandler
{
  public:
  XtandemParamSaxHandler(TandemParameters *p_tandem_parameters);
  ~XtandemParamSaxHandler();

  bool startElement(const QString &namespaceURI,
                    const QString &localName,
                    const QString &qName,
                    const QXmlAttributes &attributes);

  bool endElement(const QString &namespaceURI,
                  const QString &localName,
                  const QString &qName);

  bool startDocument();

  bool endDocument();

  bool characters(const QString &str);

  bool fatalError(const QXmlParseException &exception);
  bool error(const QXmlParseException &exception);

  QString errorString() const;

  bool isTandemParameter() const;

  private:
  bool startElement_note(QXmlAttributes attributes);
  bool endElement_note();

  private:
  TandemParameters *_p_tandem_parameters;

  std::vector<QString> _tag_stack;
  QString _errorStr;
  QString _current_text;

  QString _current_label;
  bool m_isTandemParameter = false;
};
