/**
 * \file input/condorqxmlsaxhandler.cpp
 * \date 15/9/2017
 * \author Olivier Langella
 * \brief parse condor_q XML
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "condorqxmlsaxhandler.h"
#include "../core/tandem_run/tandemcondorprocess.h"
#include <pappsomspp/pappsoexception.h>

CondorQxmlSaxHandler::CondorQxmlSaxHandler(
  TandemCondorProcess *tandem_condor_process)
{
  _is_empty              = true;
  _tandem_condor_process = tandem_condor_process;
}
CondorQxmlSaxHandler::~CondorQxmlSaxHandler()
{
}


bool
CondorQxmlSaxHandler::startElement(const QString &namespaceURI,
                                   const QString &localName,
                                   const QString &qName,
                                   const QXmlAttributes &attributes)
{
  qDebug() << "CondorQxmlSaxHandler::startElement begin" << namespaceURI << " "
           << localName << " " << qName;
  _tag_stack.push_back(qName);
  bool is_ok = true;

  try
    {
      // startElement_group

      if(qName == "c")
        {
          is_ok = startElement_c(attributes);
        }
      else if(qName == "a")
        {
          is_ok = startElement_a(attributes);
        }

      _current_text.clear();
    }
  catch(pappso::PappsoException &exception_pappso)
    {
      _errorStr = QObject::tr(
                    "ERROR in CondorQxmlSaxHandler::startElement tag "
                    "%1, PAPPSO exception:\n%2")
                    .arg(qName)
                    .arg(exception_pappso.qwhat());
      return false;
    }
  catch(std::exception &exception_std)
    {
      _errorStr = QObject::tr(
                    "ERROR in CondorQxmlSaxHandler::startElement tag "
                    "%1, std exception:\n%2")
                    .arg(qName)
                    .arg(exception_std.what());
      return false;
    }
  return is_ok;
}

bool
CondorQxmlSaxHandler::endElement(const QString &namespaceURI,
                                 const QString &localName,
                                 const QString &qName)
{

  bool is_ok = true;
  // endElement_peptide_list
  try
    {

      if(qName == "a")
        {
          is_ok = endElement_a();
        }
      else if(qName == "c")
        {
          is_ok = endElement_c();
        }
      else if(qName == "i")
        {
          is_ok = endElement_i();
        }
      else if(qName == "s")
        {
          is_ok = endElement_s();
        }
    }
  catch(pappso::PappsoException &exception_pappso)
    {
      _errorStr = QObject::tr(
                    "ERROR in CondorQxmlSaxHandler::endElement tag "
                    "%1, PAPPSO exception:\n%2")
                    .arg(qName)
                    .arg(exception_pappso.qwhat());
      return false;
    }
  catch(std::exception &exception_std)
    {
      _errorStr = QObject::tr(
                    "ERROR in CondorQxmlSaxHandler::endElement tag "
                    "%1, std exception:\n%2")
                    .arg(qName)
                    .arg(exception_std.what());
      return false;
    }

  _current_text.clear();
  _tag_stack.pop_back();

  return is_ok;
}


bool
CondorQxmlSaxHandler::error(const QXmlParseException &exception)
{
  _errorStr = QObject::tr(
                "Parse error at line %1, column %2 :\n"
                "%3")
                .arg(exception.lineNumber())
                .arg(exception.columnNumber())
                .arg(exception.message());

  return false;
}


bool
CondorQxmlSaxHandler::fatalError(const QXmlParseException &exception)
{
  _errorStr = QObject::tr(
                "Parse error at line %1, column %2 :\n"
                "%3")
                .arg(exception.lineNumber())
                .arg(exception.columnNumber())
                .arg(exception.message());
  return false;
}

QString
CondorQxmlSaxHandler::errorString() const
{
  return _errorStr;
}


bool
CondorQxmlSaxHandler::endDocument()
{

  unsigned int really_completed_jobs = 0;
  if(_is_empty)
    {
      really_completed_jobs = _tandem_condor_process->getCondorJobSize();
    }
  else
    {
      int total_jobs = 0;
      for(std::int8_t i = 0; i < 10; i++)
        {
          total_jobs += _count_status[i];
        }

      int diff_jobs = _tandem_condor_process->getCondorJobSize() - total_jobs;
      really_completed_jobs =
        _count_status[(std::int8_t)CondorJobStatus::Completed] + diff_jobs;
    }
  _count_status[(std::int8_t)CondorJobStatus::Completed] =
    really_completed_jobs;

  _tandem_condor_process->setCondorJobStatus(_count_status);
  /*
  xtandemCondorAnalysis.setJobCompleted(junexpanded, jidle, jrunning,
      jremoved, reallyCompletedJobs, jheld, jsubmission_error);

  */

  return true;
}

bool
CondorQxmlSaxHandler::startDocument()
{
  for(std::int8_t i = 0; i < 10; i++)
    {
      _count_status[i] = 0;
    }
  _is_empty = true;
  return true;
}

bool
CondorQxmlSaxHandler::characters(const QString &str)
{
  _current_text += str;
  return true;
}

bool
CondorQxmlSaxHandler::startElement_c(QXmlAttributes attributes)
{

  // <c>
  // <a n="ProcId"><i>0</i></a>
  _condor_job_status        = CondorJobStatus::Unexpanded;
  _current_proc_id          = 0;
  _is_empty                 = false;
  _current_remote_host      = "";
  _current_last_remote_host = "";
  return true;
}

bool
CondorQxmlSaxHandler::startElement_a(QXmlAttributes attributes)
{
  // logger.debug("startElementgroup begin");
  // <group label="performance parameters" type="parameters">
  _in_name = attributes.value("n");
  return true;
}

bool
CondorQxmlSaxHandler::endElement_a()
{
  _in_name = "";
  return true;
}

bool
CondorQxmlSaxHandler::endElement_i()
{
  if(_in_name == "ProcId")
    {
      _current_proc_id = _current_text.toInt();
    }
  else if(_in_name == "JobStatus")
    {
      // <a n="JobStatus"><i>2</i></a>
      _condor_job_status = static_cast<CondorJobStatus>(_current_text.toInt());
      // logger.debug(currentProcId);
    }
  return true;
}

bool
CondorQxmlSaxHandler::endElement_s()
{
  if(_in_name == "RemoteHost")
    {
      // <a n="RemoteHost"><s>slot1@proteus3</s></a>
      _current_remote_host = _current_text;
    }
  else if(_in_name == "LastRemoteHost")
    {
      // <a n="LastRemoteHost"><s>slot1@proteus4</s></a>
      _current_last_remote_host = _current_text;
    }
  return true;
}

bool
CondorQxmlSaxHandler::endElement_c()
{
  /*
   * 0 Unexpanded U 1 Idle I 2 Running R 3 Removed X 4 Completed C 5 Held
   * H 6 Submission_err E
   */
  _count_status[(std::int8_t)_condor_job_status] += 1;

  qDebug() << "currentRemoteHost:" << _current_remote_host
           << " currentLastRemoteHost" << _current_last_remote_host << " "
           << (std::int8_t)_condor_job_status;
  return true;
}

/*
package fr.inra.pappso.xtandempipeline.sax_parse;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import fr.inra.pappso.xtandempipeline.MsException.MSMSException;
import fr.inra.pappso.xtandempipeline.thread.XtandemCondorAnalysis;

public class HandlerHTCondorQueue extends SaxBaseHandler {

private static final Logger logger = Logger
    .getLogger(HandlerHTCondorQueue.class);
private Object inName;
private Integer currentProcId;
private Integer currentJobStatus;
private String currentRemoteHost;
private String currentLastRemoteHost;
private int junexpanded = 0;
private int jidle = 0;
private int jrunning = 0;
private int jremoved = 0;
private int jcompleted = 0;
private int jheld = 0;
private int jsubmission_error = 0;
private XtandemCondorAnalysis xtandemCondorAnalysis;
private boolean empty = true;

public HandlerHTCondorQueue(XtandemCondorAnalysis xtandemCondorAnalysis)
    throws MSMSException {
  super();
  this.xtandemCondorAnalysis = xtandemCondorAnalysis;
}

@Override
public void startDocument() throws SAXException {
  this.empty = true;
}


}
*/
