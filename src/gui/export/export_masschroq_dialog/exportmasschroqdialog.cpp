/**
 * \file gui/export/export_masschroq_dialog/exportmasschroqdialog.cpp
 * \date 25/01/2019
 * \author Olivier Langella
 * \brief choose ODS export options
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "exportmasschroqdialog.h"

#include "ui_export_masschroq_dialog.h"
#include "exportmasschroqdialog.h"
#include <QDebug>
#include <QSettings>
#include <QMessageBox>
#include <QFileDialog>
#include <pappsomspp/msrun/alignment/msrunretentiontime.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/pappsoexception.h>
#include "../../mainwindow.h"

ExportMasschroqDialog::ExportMasschroqDialog(MainWindow *parent,
                                             WorkerThread *p_worker)
  : QDialog(parent), ui(new Ui::ExportMasschroqDialog)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  mp_main = parent;
  ui->setupUi(this);
  this->setModal(true);

  ui->directorySelectWidget->setHidden(true);
  ui->msrunFilepathOkLabel->setHidden(true);

  ui->warningDirectoryLabel->setStyleSheet("QLabel { color : red; }");


  connect(this,
          &ExportMasschroqDialog::accepted,
          mp_main,
          &MainWindow::doAcceptedExportMasschroqDialog);

  connect(this,
          &ExportMasschroqDialog::operateFindBestMsrunForAlignment,
          p_worker,
          &WorkerThread::doFindBestMsrunForAlignment);
  connect(p_worker,
          &WorkerThread::findingBestMsrunForAlignmentFinished,
          this,
          &ExportMasschroqDialog::setBestMsrunForAlignment);


  connect(this,
          &ExportMasschroqDialog::operateCheckingMsrunFilePath,
          p_worker,
          &WorkerThread::doCheckMsrunFilePath);
  connect(p_worker,
          &WorkerThread::checkingMsrunFilePathFinished,
          this,
          &ExportMasschroqDialog::setCheckMsrunFilePathOk);


  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

ExportMasschroqDialog::~ExportMasschroqDialog()
{
  delete ui;
}

void
ExportMasschroqDialog::setProjectSPtr(ProjectSp p_project)
{
  msp_project = p_project;
  fillMsrunListComboBox();
}

void
ExportMasschroqDialog::setMasschroqFileParameters(
  const MasschroqFileParameters &params)
{
  ZivyParams zivy_params;
  zivy_params.loadSettings();

  ui->zivyParamWidget->setZivyParams(zivy_params);

  ui->outputFileEdit->setText(params.result_file_name);
  ui->outputFileFormatComboBox->setCurrentText("ODS");
  if(params.result_file_format == TableFileFormat::tsv)
    {
      ui->outputFileFormatComboBox->setCurrentText("TSV");
    }

  ui->comparFileGroupBox->setChecked(params.export_compar_file);

  ui->comparFileEdit->setText(params.compar_file_name);
  ui->comparFileFormatComboBox->setCurrentText("ODS");
  if(params.compar_file_format == TableFileFormat::tsv)
    {
      ui->comparFileFormatComboBox->setCurrentText("TSV");
    }


  ui->timeCorrectionGroupBox->setChecked(params.write_alignment_times);
  ui->timeCorrectionDirectoryEdit->setText(params.alignment_times_directory);

  ui->ms2TendencySpinBox->setValue(params.ms2_tendency_half_window);
  ui->ms2SmoothingSpinBox->setValue(params.ms2_smoothing_half_window);
  ui->ms1SmoothingSpinBox->setValue(params.ms1_smoothing_half_window);

  ui->xicRangeWidget->setPrecision(params.xic_extraction_range);
}


MasschroqFileParameters
ExportMasschroqDialog::getMasschroqFileParameters() const
{
  MasschroqFileParameters params;

  params.result_file_name   = ui->outputFileEdit->text();
  params.result_file_format = TableFileFormat::ods;
  if(ui->outputFileFormatComboBox->currentText() == "TSV")
    {
      params.result_file_format = TableFileFormat::tsv;
    }

  params.export_compar_file = ui->comparFileGroupBox->isChecked();
  params.compar_file_name   = ui->comparFileEdit->text();
  params.compar_file_format = TableFileFormat::ods;
  if(ui->comparFileFormatComboBox->currentText() == "TSV")
    {
      params.compar_file_format = TableFileFormat::tsv;
    }


  params.write_alignment_times     = ui->timeCorrectionGroupBox->isChecked();
  params.alignment_times_directory = ui->timeCorrectionDirectoryEdit->text();

  params.ms2_tendency_half_window  = ui->ms2TendencySpinBox->value();
  params.ms2_smoothing_half_window = ui->ms2SmoothingSpinBox->value();
  params.ms1_smoothing_half_window = ui->ms1SmoothingSpinBox->value();

  params.xic_extraction_range = ui->xicRangeWidget->getPrecision();

  params.xic_extraction_method =
    ui->xic_extraction_method_widget->getXicExtractionMethod();


  int index = ui->msrun_list_combo_box->currentIndex();
  if(index != -1)
    {
      MsRunSp msrun_sp =
        qvariant_cast<MsRunSp>(ui->msrun_list_combo_box->itemData(index));
      params.msrun_alignment_reference = msrun_sp;
    }


  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << (int)params.xic_extraction_method;
  return params;
}


void
ExportMasschroqDialog::reject()
{
  msp_project = nullptr;
  QDialog::reject();
}

void
ExportMasschroqDialog::accept()
{
  QString error_message;
  if(ui->outputFileEdit->text().isEmpty())
    {
      error_message = QString(tr("MassChroqML result file name must be set"));
    }
  if(ui->comparFileGroupBox->isChecked())
    {
      if(ui->comparFileEdit->text().isEmpty())
        {
          error_message =
            QString(tr("MassChroqML compar file name must be set"));
        }
    }

  if(ui->timeCorrectionGroupBox->isChecked())
    {
      if(ui->timeCorrectionDirectoryEdit->text().isEmpty())
        {
          error_message = QString(tr(
            "the directory name to write alignment corrections must be set"));
        }
    }

  if((ui->xicRangeWidget->getPrecision()->unit() ==
      pappso::PrecisionUnit::ppm) ||
     (ui->xicRangeWidget->getPrecision()->unit() ==
      pappso::PrecisionUnit::dalton))
    { // ok
    }
  else
    {
      error_message =
        QString(tr("MassChroQ can only work with ppm or dalton units. Please "
                   "change the XIC extraction precision unit."));
    }

  if(error_message.isEmpty())
    {
      ZivyParams zivy_params = ui->zivyParamWidget->getZivyParams();
      zivy_params.saveSettings();
      msp_project = nullptr;
      QDialog::accept();
    }
  else
    {
      QMessageBox msgBox;
      msgBox.setWindowTitle(tr("MassChroQ parameter problem"));
      msgBox.setText(error_message);
      msgBox.setIcon(QMessageBox::Critical);
      msgBox.exec();
    }
}


void
ExportMasschroqDialog::doCheckMsrunFilepath()
{

  mp_main->showWaitingMessage(tr("Checking MSrun files path"));
  emit operateCheckingMsrunFilePath(msp_project);
}

void
ExportMasschroqDialog::doBrowseMsrunDirectory()
{

  QSettings settings;
  QString path     = settings.value("path/mzdatadir", "").toString();
  QString filename = QFileDialog::getExistingDirectory(
    this, tr("Choose directory to look for MS runs"), QString("%1").arg(path));

  if(!filename.isEmpty())
    {
      QDir parent(filename);
      // parent.cdUp();
      settings.setValue("path/mzdatadir", parent.canonicalPath());
      doCheckMsrunFilepath();
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
               << parent.absolutePath();
    }
}


void
ExportMasschroqDialog::doFindBestMsrunForAlignment()
{
  mp_main->showWaitingMessage(tr("Looking for MSrun reference"));
  emit operateFindBestMsrunForAlignment(msp_project);
}

void
ExportMasschroqDialog::setBestMsrunForAlignment(MsRunSp best_msrun_sp)
{

  mp_main->hideWaitingMessage();
  if(best_msrun_sp != nullptr)
    {
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
               << best_msrun_sp.get()->getXmlId();

      for(int index = 0; index < ui->msrun_list_combo_box->count(); index++)
        {
          MsRunSp msrun_sp =
            qvariant_cast<MsRunSp>(ui->msrun_list_combo_box->itemData(index));
          if(msrun_sp.get() == best_msrun_sp.get())
            {
              ui->msrun_list_combo_box->setCurrentIndex(index);
            }
        }
    }
}

void
ExportMasschroqDialog::setCheckMsrunFilePathOk(MsRunSp msrun_sp)
{
  mp_main->hideWaitingMessage();
  if(msrun_sp == nullptr)
    {
      ui->directorySelectWidget->setHidden(true);

      ui->msrunFilepathOkLabel->setText(tr("All MS run files found"));
      ui->msrunFilepathOkLabel->setHidden(false);
    }
  else
    {

      ui->warningDirectoryLabel->setText(
        tr("\"%1\" not found : Please choose the directory to look for this MS "
           "run file.")
          .arg(msrun_sp.get()->getFileName()));
      ui->directorySelectWidget->setHidden(false);
    }
}

void
ExportMasschroqDialog::fillMsrunListComboBox()
{
  ui->msrun_list_combo_box->clear();
  if(msp_project != nullptr)
    {

      int index = -1;
      for(auto &msrun_sp : msp_project->getMsRunStore().getMsRunList())
        {
          index = 0;
          ui->msrun_list_combo_box->addItem(
            QString("%1").arg(msrun_sp.get()->getSampleName()),
            QVariant::fromValue(msrun_sp));
        }
      if(index != -1)
        { // -1 for not found
          ui->msrun_list_combo_box->setCurrentIndex(index);
        }
    }
}
