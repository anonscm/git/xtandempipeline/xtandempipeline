/**
 * \file gui/export/export_masschroq_dialog/masschroqfileparameters.cpp
 * \date 25/01/2019
 * \author Olivier Langella
 * \brief all needed informations to write a MassChroqML file
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "masschroqfileparameters.h"
#include <QSettings>

void
MasschroqFileParameters::save() const
{
  QSettings settings;
  settings.setValue("export_masschroqml/result_file_name",
                    QString("%1").arg(result_file_name));
  settings.setValue("export_masschroqml/result_file_format",
                    (std::uint8_t)result_file_format);


  settings.setValue("export_masschroqml/export_compar_file",
                    QString("%1").arg(export_compar_file));
  settings.setValue("export_masschroqml/compar_file_name",
                    QString("%1").arg(compar_file_name));
  settings.setValue("export_masschroqml/compar_file_format",
                    (std::uint8_t)compar_file_format);


  settings.setValue("export_masschroqml/write_alignment_times",
                    write_alignment_times);
  settings.setValue("export_masschroqml/alignment_times_directory",
                    alignment_times_directory);


  settings.setValue("export_masschroqml/ms2_tendency_half_window",
                    ms2_tendency_half_window);
  settings.setValue("export_masschroqml/ms2_smoothing_half_window",
                    ms2_smoothing_half_window);
  settings.setValue("export_masschroqml/ms1_smoothing_half_window",
                    ms1_smoothing_half_window);


  settings.setValue("export_masschroqml/xic_extraction_range",
                    xic_extraction_range->toString());

  settings.setValue("export_masschroqml/xic_extraction_method",
                    QString("%1").arg((std::uint8_t)xic_extraction_method));
}
void
MasschroqFileParameters::load()
{
  QSettings settings;
  result_file_name =
    settings.value("export_masschroqml/result_file_name", QString("results"))
      .toString();
  result_file_format =
    (TableFileFormat)settings
      .value("export_masschroqml/result_file_format",
             QString("%1").arg((std::uint8_t)TableFileFormat::ods))
      .toUInt();


  export_compar_file =
    settings
      .value("export_masschroqml/compar_file_name", QString("%1").arg(true))
      .toBool();
  compar_file_name =
    settings
      .value("export_masschroqml/compar_file_name", QString("compar_results"))
      .toString();
  compar_file_format =
    (TableFileFormat)settings
      .value("export_masschroqml/compar_file_format",
             QString("%1").arg((std::uint8_t)TableFileFormat::ods))
      .toUInt();


  write_alignment_times = settings
                            .value("export_masschroqml/write_alignment_times",
                                   QString("%1").arg(false))
                            .toBool();

  alignment_times_directory =
    settings
      .value("export_masschroqml/alignment_times_directory", QString("times.d"))
      .toString();

  ms2_tendency_half_window =
    settings.value("export_masschroqml/ms2_tendency_half_window", "10")
      .toUInt();

  ms2_smoothing_half_window =
    settings.value("export_masschroqml/ms2_smoothing_half_window", "15")
      .toUInt();

  ms1_smoothing_half_window =
    settings.value("export_masschroqml/ms1_smoothing_half_window", "0")
      .toUInt();

  xic_extraction_range = pappso::PrecisionFactory::fromString(
    settings.value("export_masschroqml/xic_extraction_range", "10 ppm")
      .toString());

  xic_extraction_method =
    (pappso::XicExtractMethod)settings
      .value("export_masschroqml/xic_extraction_method",
             QString("%1").arg((std::uint8_t)pappso::XicExtractMethod::max))
      .toUInt();
}
