/**
 * \file gui/ptm_island_list_window/ptmislandproxymodel.cpp
 * \date 30/5/2017
 * \author Olivier Langella
 * \brief display all ptm islands
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "ptmislandproxymodel.h"
#include "ptmislandtablemodel.h"
#include "ptmislandlistwindow.h"
#include "../../grouping/ptm/ptmgroupingexperiment.h"
#include "../../grouping/ptm/ptmisland.h"
#include <pappsomspp/pappsoexception.h>

PtmIslandProxyModel::PtmIslandProxyModel(
  PtmIslandListWindow *p_ptm_island_list_window,
  PtmIslandTableModel *ptm_table_model_p)
{
  _p_ptm_island_list_window = p_ptm_island_list_window;
  _p_ptm_island_table_model = ptm_table_model_p;
  _ptm_search_string        = "";
  _search_on                = "accession";
}

PtmIslandProxyModel::~PtmIslandProxyModel()
{
}

bool
PtmIslandProxyModel::filterAcceptsRow(int source_row,
                                      const QModelIndex &source_parent) const
{
  try
    {
      // qDebug() << "ProteinTableProxyModel::filterAcceptsRow begin " <<
      // source_row;
      PtmIslandSp sp_ptm_island =
        _p_ptm_island_table_model->getPtmGroupingExperiment()
          ->getPtmIslandList()
          .at(source_row);
      // qDebug() << "ProteinTableProxyModel::filterAcceptsRow protein_match "
      // << source_row;

      if(!_ptm_search_string.isEmpty())
        {
          if(_search_on == "accession")
            {
              if(!sp_ptm_island.get()
                    ->getProteinMatch()
                    ->getProteinXtpSp()
                    .get()
                    ->getAccession()
                    .contains(_ptm_search_string))
                {
                  return false;
                }
            }
          else if(_search_on == "group")
            {
              if(!sp_ptm_island.get()->getGroupingId().startsWith(
                   QString("ptm%1").arg(_ptm_search_string)))
                {
                  return false;
                }
            }
          else if(_search_on == "sequence")
            {
              if(!sp_ptm_island.get()
                    ->getProteinMatch()
                    ->getProteinXtpSp()
                    .get()
                    ->getSequence()
                    .contains(_ptm_search_string))
                {
                  return false;
                }
            }
          else if(_search_on == "peptide")
            {
              QString peptide_search_string =
                QString(_ptm_search_string).replace("L", "I");
              for(const PeptideMatch &p_peptide_match :
                  sp_ptm_island.get()->getProteinMatch()->getPeptideMatchList())
                {
                  if(p_peptide_match.getPeptideEvidence()
                       ->getPeptideXtpSp()
                       .get()
                       ->getSequenceLi()
                       .contains(peptide_search_string))
                    {
                      return true;
                    }
                }
              return false;
            }

          else if(_search_on == "description")
            {
              // description
              if(!sp_ptm_island.get()
                    ->getProteinMatch()
                    ->getProteinXtpSp()
                    .get()
                    ->getDescription()
                    .contains(_ptm_search_string))
                {
                  return false;
                }
            }
        }
      if(sp_ptm_island != nullptr)
        {
          return true;
        }
      return false;
    }

  catch(pappso::PappsoException &exception_pappso)
    {
      // QMessageBox::warning(this,
      //                     tr("Error in ProteinTableModel::acceptRow :"),
      //                     exception_pappso.qwhat());
      qDebug() << "Error in PtmIslandProxyModel::acceptRow :"
               << exception_pappso.qwhat();
    }
  catch(std::exception &exception_std)
    {
      // QMessageBox::warning(this,
      //                    tr("Error in ProteinTableModel::acceptRow :"),
      //                    exception_std.what());
      qDebug() << "Error in PtmIslandProxyModel::acceptRow :"
               << exception_std.what();
    }

  return true;

  // return true;
}


void
PtmIslandProxyModel::onTableClicked(const QModelIndex &index)
{
  qDebug() << "PtmIslandProxyModel::onTableClicked begin " << index.row();
  qDebug() << "PtmIslandProxyModel::onTableClicked begin "
           << this->mapToSource(index).row();

  //_protein_table_model_p->onTableClicked(this->mapToSource(index));
  QModelIndex source_index(this->mapToSource(index));
  int row = source_index.row();
  int col = source_index.column();

  // ProteinMatch* p_protein_match =
  // _p_ptm_island_table_model->getIdentificationGroup()->getProteinMatchList().at(row);

  if((col == (std::int8_t)PtmIslandListColumn::accession) ||
     (col == (std::int8_t)PtmIslandListColumn::description))
    {
      //_p_ptm_island_table_model->askPtmProteinDetailView(p_protein_match);
    }
  else
    {
      _p_ptm_island_list_window->askViewPtmPeptideList(
        _p_ptm_island_table_model->getPtmGroupingExperiment()
          ->getPtmIslandList()
          .at(row)
          .get());
    }

  qDebug() << "PtmIslandProxyModel::onTableClicked end " << index.row();
}


void
PtmIslandProxyModel::setSearchOn(QString search_on)
{
  _search_on = search_on;
}

void
PtmIslandProxyModel::setPtmSearchString(QString ptm_search_string)
{
  _ptm_search_string = ptm_search_string;
}
