
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef CHOOSEMODIFICATIONDIALOG_H
#define CHOOSEMODIFICATIONDIALOG_H

#include <QDialog>
#include <QStandardItemModel>
#include <pappsomspp/mzrange.h>
#include <pappsomspp/amino_acid/aamodification.h>


namespace Ui
{
class ChooseModificationDialog;
}

class ChooseModificationDialog : public QDialog
{
  Q_OBJECT

  public:
  explicit ChooseModificationDialog(QWidget *parent);
  ~ChooseModificationDialog();

  void setMassRange(const pappso::MzRange &massrange);
  public slots:
  void ItemClicked(QModelIndex index);
  void ItemDoubleClicked(QModelIndex index);

  pappso::AaModificationP getSelectedModification() const;

  private:
  Ui::ChooseModificationDialog *ui;
  QStandardItemModel *_p_modification_str_li = nullptr;
  pappso::AaModificationP _selected_modification;
};

#endif // CHOOSEMODIFICATIONDIALOG_H
