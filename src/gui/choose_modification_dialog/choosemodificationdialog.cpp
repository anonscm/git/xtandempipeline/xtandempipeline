
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "choosemodificationdialog.h"

#include "ui_choose_modification_dialog.h"
#include <QDebug>
#include <QDesktopServices>
#include <pappsomspp/obo/filterobopsimodtermlabel.h>
#include <pappsomspp/obo/filterobopsimodsink.h>
#include <pappsomspp/obo/filterobopsimodtermdiffmono.h>
#include <pappsomspp/obo/filterobopsimodtermlabel.h>
#include <pappsomspp/obo/filterobopsimodtermname.h>
#include "../../utils/utils.h"

ChooseModificationDialog::ChooseModificationDialog(QWidget *parent)
  : QDialog(parent), ui(new Ui::ChooseModificationDialog)
{
  qDebug() << "ChooseModificationDialog::ChooseModificationDialog begin";
  ui->setupUi(this);
  this->setModal(true);

  _p_modification_str_li = new QStandardItemModel();
  ui->modification_listview->setModel(_p_modification_str_li);
  // param.setFilterCrossSamplePeptideNumber(settings.value("automatic_filter/cross_sample",
  // "true").toBool());

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(ui->modification_listview,
          &QListView::clicked,
          this,
          &ChooseModificationDialog::ItemClicked);
  connect(ui->modification_listview,
          &QListView::doubleClicked,
          this,
          &ChooseModificationDialog::ItemDoubleClicked);
#else
  // Qt4 code

  connect(ui->modification_listview,
          SIGNAL(clicked(const QModelIndex)),
          this,
          SLOT(ItemClicked(QModelIndex)));
  connect(ui->modification_listview,
          SIGNAL(doubleClicked(const QModelIndex)),
          this,
          SLOT(ItemDoubleClicked(QModelIndex)));

#endif
}

ChooseModificationDialog::~ChooseModificationDialog()
{
  qDebug() << "ChooseModificationDialog::~ChooseModificationDialog";
  delete ui;

  qDebug() << "ChooseModificationDialog::~ChooseModificationDialog end";
}

void
ChooseModificationDialog::setMassRange(const pappso::MzRange &massrange)
{

  qDebug() << "ChooseModificationDialog::setMassRange " << massrange.toString();
  _selected_modification = nullptr;
  _p_modification_str_li->removeRows(0, _p_modification_str_li->rowCount());
  pappso::FilterOboPsiModSink term_list;

  pappso::OboPsiModHandlerInterface *last_men_standing = &term_list;
  pappso::FilterOboPsiModTermDiffMono *p_filter_mass   = nullptr;
  // FilterOboPsiModTermLabel * p_filter_label = nullptr;
  // FilterOboPsiModTermName * p_filter_name = nullptr;

  p_filter_mass =
    new pappso::FilterOboPsiModTermDiffMono(*last_men_standing, massrange);
  pappso::OboPsiMod psimodb(*p_filter_mass);
  for(pappso::OboPsiModTerm term : term_list.getOboPsiModTermList())
    {

      QStandardItem *item;
      item = new QStandardItem(
        QString("[%1] %2").arg(term.m_accession).arg(term.m_name));
      item->setEditable(false);
      _p_modification_str_li->appendRow(item);
      item->setData(QVariant(QString("%1").arg(term.m_accession)),
                    Qt::UserRole);
    }
}


void
ChooseModificationDialog::ItemDoubleClicked(QModelIndex index)
{
  qDebug() << "ChooseModificationDialog::ItemClicked data="
           << index.data().toString();
  qDebug() << "ChooseModificationDialog::ItemClicked acc="
           << _p_modification_str_li->data(index, Qt::UserRole).toString();
  QDesktopServices::openUrl(Utils::getOlsUrl(
    _p_modification_str_li->data(index, Qt::UserRole).toString()));
}

void
ChooseModificationDialog::ItemClicked(QModelIndex index)
{
  qDebug() << "ChooseModificationDialog::ItemClicked data="
           << index.data().toString();
  qDebug() << "ChooseModificationDialog::ItemClicked acc="
           << _p_modification_str_li->data(index, Qt::UserRole).toString();
  qDebug() << "ChooseModificationDialog::ItemClicked str="
           << _p_modification_str_li->data(index, Qt::DisplayRole).toString();
  pappso::AaModificationP modification =
    pappso::AaModification::getInstance(index.data(Qt::UserRole).toString());
  _selected_modification = modification;
}

pappso::AaModificationP
ChooseModificationDialog::getSelectedModification() const
{
  return _selected_modification;
}
