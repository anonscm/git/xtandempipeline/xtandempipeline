/**
 * \file /gui/workerthread.h
 * \date 8/5/2017
 * \author Olivier Langella
 * \brief worker thread
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QThread>
#include <QCloseEvent>
#include "../core/automaticfilterparameters.h"
#include "../core/project.h"
#include "../utils/workmonitor.h"
#include "../core/tandem_run/tandemrunbatch.h"
#include "../gui/export/export_masschroq_dialog/masschroqfileparameters.h"

class MainWindow;
class ProjectWindow;

class WorkerThread : public QObject
{
  Q_OBJECT

  public:
  WorkerThread(MainWindow *parent);
  WorkerThread(ProjectWindow *parent);
  virtual ~WorkerThread();


  protected:
  void closeEvent(QCloseEvent *event);
  void checkMsrunFilePath(std::vector<MsRunSp> msruns);
  MsRunSp chooseBestMsRun(std::vector<MsRunSp> msruns);

  public slots:
  void doXpipFileLoad(QString filename);
  void doLoadingResults(bool is_individual,
                        AutomaticFilterParameters param,
                        QStringList file_list);
  void doWritingXpipFile(QString filename, ProjectSp project_sp);
  void doWritingOdsFile(QString filename, QString format, ProjectSp project_sp);
  void doWritingMassChroqFile(QString filename,
                              ProjectSp project_sp,
                              MasschroqFileParameters params);
  void doWritingMassChroqPrmFile(QString filename, ProjectSp project_sp);
  void doWritingProticFile(QString filename, ProjectSp project_sp);
  void doWritingMcqrSpectralCountFile(QString filename, ProjectSp project_sp);
  void doWritingFastaFile(QString filename,
                          ProjectSp project_sp,
                          ExportFastaType type);
  void doGrouping(ProjectSp project_sp);
  void
  doGroupingOnIdentification(IdentificationGroup *p_identification_group,
                             ContaminantRemovalMode contaminant_removal_mode,
                             GroupingType grouping_type);
  void
  doPtmGroupingOnIdentification(IdentificationGroup *p_identification_group);
  void doRunningXtandem(TandemRunBatch tandem_run_batch);
  void doFindBestMsrunForAlignment(ProjectSp project_sp);
  void doFindBestMsrunInAlignmentGroup(MsRunAlignmentGroupSp group);
  void doCheckMsrunFilePath(ProjectSp project_sp);


  signals:
  void loadingMessage(QString message);
  void projectReady(ProjectSp project_sp);
  void loadingResultsFinished(ProjectSp project_sp);
  void projectNotReady(QString error);
  void groupingFinished();
  void
  groupingOnIdentificationFinished(IdentificationGroup *p_identification_group);
  void ptmGroupingOnIdentificationFinished(
    IdentificationGroup *p_identification_group);
  void operationFinished();
  void operationFailed(QString error);
  void findingBestMsrunForAlignmentFinished(MsRunSp msrun_sp);
  void bestMsrunForAlignmentGroupFinished(MsRunSp msrun_sp);
  void checkingMsrunFilePathFinished(MsRunSp msrun_sp);

  private:
  WorkMonitor *_p_work_monitor;
  MainWindow *_p_main_window;
};
