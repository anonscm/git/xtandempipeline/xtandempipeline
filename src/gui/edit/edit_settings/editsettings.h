/**
 * \file gui/edit/edit_settings/editsettings.h
 * \date 23/2/2019
 * \author Olivier Langella
 * \brief dialog box to edit global settings
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QDialog>
#include <QFileInfo>

namespace Ui
{
class EditSettingsDialog;
}

class EditSettings : public QDialog
{
  Q_OBJECT

  public:
  explicit EditSettings(QWidget *parent = 0);
  ~EditSettings();

  protected:
  void done(int r) override;
  bool testTandemWrapperWorks(QString tandemwrapper_path);

  public slots:
  void selectTandemWrapperPath();
  void selectTmpDirPath();

  private:
  Ui::EditSettingsDialog *ui;
};
