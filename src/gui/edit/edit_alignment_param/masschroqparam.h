#include <core/alignmentgroup.h>

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *    Thomas Renne <thomas.renne@u-psud.fr>. - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "ui_masschroq_param_view.h"
#include <gui/workerthread.h>

namespace Ui
{
class MassChroQParamView;
}

class MassChroQParamWindow : public QWidget
{
  Q_OBJECT

  public:
  MassChroQParamWindow(ProjectWindow *parent);
  ~MassChroQParamWindow();
  void setAlignmentGroup(MsRunAlignmentGroupSp alignment_group);

  public slots:
  void doFindBestMsRunInGroup();
  void doBestMsRunFound(MsRunSp msrun_sp);

  signals:
  void
  operateFindBestMsrunForAlignmentGroup(MsRunAlignmentGroupSp alignment_group);

  private:
  Ui::MassChroQParamView *ui;
  ProjectWindow *mp_projectWindow;
  MsRunAlignmentGroupSp msp_alignmentGroup;
};
