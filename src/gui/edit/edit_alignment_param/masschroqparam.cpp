
/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *    Thomas Renne <thomas.renne@u-psud.fr>. - initial API and
 *implementation
 ******************************************************************************/

#include <QWidget>

#include "ui_masschroq_param_view.h"
#include "masschroqparam.h"
#include "../../project_view/projectwindow.h"

MassChroQParamWindow::MassChroQParamWindow(ProjectWindow *parent)
  : QWidget(), ui(new Ui::MassChroQParamView)
{
  mp_projectWindow = parent;
  ui->setupUi(this);
  connect(this,
          &MassChroQParamWindow::operateFindBestMsrunForAlignmentGroup,
          mp_projectWindow,
          &ProjectWindow::doFindBestMsRunInAlignmentGroup);
  connect(mp_projectWindow,
          &ProjectWindow::bestMsRunFound,
          this,
          &MassChroQParamWindow::doBestMsRunFound);
}


MassChroQParamWindow::~MassChroQParamWindow()
{
}

void
MassChroQParamWindow::setAlignmentGroup(MsRunAlignmentGroupSp alignment_group)
{
  msp_alignmentGroup = alignment_group;
  ui->msruns_list->clear();
  foreach(MsRunSp ms_run, msp_alignmentGroup->getMsRunsInAlignmentGroup())
    {
      QListWidgetItem *new_item = new QListWidgetItem(ms_run->getSampleName());
      ui->msruns_list->insertItem(0, new_item);
    }
  MsRunSp reference = msp_alignmentGroup->getMsRunReference();
  if(reference != nullptr)
    {
      ui->reference_line->setText(reference->getSampleName());
    }
}

void
MassChroQParamWindow::doFindBestMsRunInGroup()
{
  //->showWaitingMessage(tr("Looking for MSrun reference"));
  emit operateFindBestMsrunForAlignmentGroup(msp_alignmentGroup);
}

void
MassChroQParamWindow::doBestMsRunFound(MsRunSp msrun_sp)
{
  try
    {
      if(msp_alignmentGroup.get()->getMsRunReference() != nullptr)
        {
          if(msp_alignmentGroup.get()->getMsRunReference()->getXmlId() ==
             msrun_sp->getXmlId())
            {
              QMessageBox::information(this,
                                       tr("job finished"),
                                       tr("The best MsRun found is the same as "
                                          "the existing reference"));
            }
          else
            {
              int rep = QMessageBox::warning(
                this,
                tr("job finished"),
                tr(
                  "The best MsRun found:\n %1\n is different from the existing "
                  "reference.\n Do you want to replace it?")
                  .arg(msrun_sp->getSampleName()),
                QMessageBox::Yes | QMessageBox::No);

              switch(rep)
                {
                  case QMessageBox::Yes:
                    msp_alignmentGroup.get()->setMsRunReference(msrun_sp);
                    ui->reference_line->setText(msrun_sp->getSampleName());
                    break;
                  case QMessageBox::No:
                    break;
                }
            }
        }
      else
        {
          QMessageBox::information(
            this,
            tr("job finished"),
            tr("%1 is the best MsRun found").arg(msrun_sp->getSampleName()));
          msp_alignmentGroup.get()->setMsRunReference(msrun_sp);
          ui->reference_line->setText(msrun_sp->getSampleName());
        }
    }
  catch(pappso::PappsoException &e)
    {
    }
}
