
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "editmodifications.h"
#include "../../project_view/projectwindow.h"
#include "../../../utils/utils.h"
#include <QDesktopServices>

#include "ui_edit_modifications.h"

EditModifications::EditModifications(ProjectWindow *parent)
  : QMainWindow(parent), ui(new Ui::EditModificationView)
{
  ui->setupUi(this);
  _project_window        = parent;
  _p_modification_str_li = new QStandardItemModel();
  ui->modification_list_view->setModel(_p_modification_str_li);

  _p_browse_modification_dialog = new ChooseModificationDialog(this);


#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(ui->modification_list_view,
          &QListView::clicked,
          this,
          &EditModifications::ItemClicked);
  connect(ui->modification_list_view,
          &QListView::doubleClicked,
          this,
          &EditModifications::ItemDoubleClicked);

  connect(_p_browse_modification_dialog,
          &ChooseModificationDialog::accepted,
          this,
          &EditModifications::doAcceptedBrowseDialog);
  connect(this,
          &EditModifications::projectStatusChanged,
          _project_window,
          &ProjectWindow::doProjectStatusChanged);
#else

  connect(ui->modification_list_view,
          SIGNAL(clicked(const QModelIndex)),
          this,
          SLOT(ItemClicked(QModelIndex)));
  connect(ui->modification_list_view,
          SIGNAL(doubleClicked(const QModelIndex)),
          this,
          SLOT(ItemDoubleClicked(QModelIndex)));

  connect(_p_browse_modification_dialog,
          SIGNAL(accepted()),
          this,
          SLOT(doAcceptedBrowseDialog()));

#endif
}

EditModifications::~EditModifications()
{
  qDebug() << "EditModifications::~EditModifications";

  delete ui;
  // delete _project_window;
  delete _p_browse_modification_dialog;
  delete _p_modification_str_li;
  qDebug() << "EditModifications::~EditModifications end";
}
void
EditModifications::setProjectSp(ProjectSp project_sp)
{

  _project_sp = project_sp;
  ui->selected_modification_groupbox->setVisible(false);
  _p_modification_str_li->removeRows(0, _p_modification_str_li->rowCount());

  std::set<pappso::AaModificationP> mod_list =
    project_sp.get()->getPeptideStore().getModificationCollection();
  //_p_modification_str_li->insertRows(0, mod_list.size());
  for(pappso::AaModificationP modification : mod_list)
    {
      if(modification->isInternal())
        {
        }
      else
        {
          QStandardItem *item;
          item = new QStandardItem(QString("[%1] %2")
                                     .arg(modification->getAccession())
                                     .arg(modification->getName()));
          item->setEditable(false);
          _p_modification_str_li->appendRow(item);
          item->setData(
            QVariant(QString("%1").arg(modification->getAccession())),
            Qt::UserRole);
        }
    }
}


void
EditModifications::ItemDoubleClicked(QModelIndex index)
{
  qDebug() << "EditModifications::ItemClicked data=" << index.data().toString();
  qDebug() << "EditModifications::ItemClicked acc="
           << _p_modification_str_li->data(index, Qt::UserRole).toString();
  QDesktopServices::openUrl(Utils::getOlsUrl(
    _p_modification_str_li->data(index, Qt::UserRole).toString()));
}

void
EditModifications::ItemClicked(QModelIndex index)
{
  qDebug() << "EditModifications::ItemClicked data=" << index.data().toString();
  qDebug() << "EditModifications::ItemClicked acc="
           << _p_modification_str_li->data(index, Qt::UserRole).toString();
  qDebug() << "EditModifications::ItemClicked str="
           << _p_modification_str_li->data(index, Qt::DisplayRole).toString();
  pappso::AaModificationP modification =
    pappso::AaModification::getInstance(index.data(Qt::UserRole).toString());
  setSelectedModification(modification);
  setReplaceModification(nullptr);
}
void
EditModifications::setReplaceModification(pappso::AaModificationP modification)
{
  _replace_modification = modification;
  if(_replace_modification == nullptr)
    {
      ui->replace_accession_label->setText("");
      ui->replace_description_label->setText("");
      ui->replace_delta_label->setText("");
    }
  else
    {
      ui->replace_accession_label->setText(
        _replace_modification->getAccession());
      ui->replace_description_label->setText(_replace_modification->getName());
      ui->replace_delta_label->setText(
        QString::number(_replace_modification->getMass(), 'f', 10));
    }
}
void
EditModifications::setSelectedModification(pappso::AaModificationP modification)
{
  qDebug() << "EditModifications::setSelectedModification acc="
           << modification->getAccession();
  _selected_modification = modification;
  ui->selected_modification_groupbox->setVisible(true);
  ui->accession_label->setText(modification->getAccession());
  ui->description_label->setText(modification->getName());
  ui->delta_label->setText(QString::number(modification->getMass(), 'f', 10));
}


void
EditModifications::doActionBrowse()
{
  qDebug() << "EditModifications::doActionBrowse begin";
  if(_selected_modification != nullptr)
    {
      pappso::MzRange massrange(
        _selected_modification->getMass(),
        pappso::PrecisionFactory::getDaltonInstance(0.05));
      _p_browse_modification_dialog->setMassRange(massrange);
      _p_browse_modification_dialog->show();
      _p_browse_modification_dialog->raise();
      _p_browse_modification_dialog->activateWindow();
    }
  qDebug() << "EditModifications::doActionBrowse end";
}

void
EditModifications::doActionReplace()
{
  qDebug() << "EditModifications::doActionReplace begin";
  if(_selected_modification != nullptr)
    {
      if(_replace_modification != nullptr)
        {
          _project_sp.get()->getPeptideStore().replaceModification(
            _selected_modification, _replace_modification);

          _project_window->doAutomaticFilterParametersChanged(
            _project_sp.get()->getAutomaticFilterParameters());
          setProjectSp(_project_sp);
          emit projectStatusChanged();
        }
    }

  qDebug() << "EditModifications::doActionReplace end";
}


void
EditModifications::doAcceptedBrowseDialog()
{
  setReplaceModification(
    _p_browse_modification_dialog->getSelectedModification());
}
