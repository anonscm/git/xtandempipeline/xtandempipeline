/**
 * \file /gui/tandem_run_dialog/tandem_run_dialog.h
 * \date 31/8/2017
 * \author Olivier Langella
 * \brief dialog window to launch tandem process
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#pragma once

#include <QDialog>
#include <QStringListModel>
#include "../../core/tandem_run/tandemrunbatch.h"
#include "../edit/edit_tandem_preset_dialog/edittandempresetdialog.h"

namespace Ui
{
class TandemRunDialog;
}


class TandemRunDialog : public QDialog
{
  Q_OBJECT

  public:
  explicit TandemRunDialog(QWidget *parent);
  ~TandemRunDialog();

  TandemRunBatch getTandemRunBatch() const;
  void reset();
  public slots:
  void exeGroupBoxClicked(bool clicked);
  void selectFastaFiles();
  void clearFastaFiles();
  void selectXtandemExe();
  void selectMzFiles();
  void clearMzFiles();
  void selectOutputDirectory();
  void selectPresetDirectory();
  void editPresets();
  void setPresetName(QString preset_name);
  void acceptPresetEdit();
  void rejectPresetEdit();
  signals:


  protected:
  void done(int r) override;

  private:
  void fillPresetComboBox();
  void fillTandemBinPath(const QString &tandem_bin_path, bool popup_firefox);
  bool checkXtandemBin();

  private:
  Ui::TandemRunDialog *ui;
  EditTandemPresetDialog *_p_preset_dialog = nullptr;
  QStringListModel *_p_fasta_file_list;
  QStringListModel *_p_mz_file_list;
  QString _previous_preset_file;
};
