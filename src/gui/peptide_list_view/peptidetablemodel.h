
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/
#pragma once

#include <QAbstractTableModel>
#include "../../core/project.h"


/** \def PeptideListColumn list of available fields to display in peptide list
 *
 */

enum class PeptideListColumn : std::int8_t
{
  checked                           = 0, ///< manual checked
  peptide_grouping_id               = 1, ///< manual checked
  engine                            = 2,
  sample                            = 3,
  scan                              = 4,
  rtmin                             = 5,
  rt                                = 6,
  charge                            = 7,
  experimental_mz                   = 8,
  sequence_nter                     = 9,
  sequence                          = 10,
  sequence_cter                     = 11,
  modifs                            = 12,
  label                             = 13,
  start                             = 14,
  length                            = 15,
  used                              = 16,
  subgroups                         = 17,
  Evalue                            = 18,
  qvalue                            = 19,
  experimental_mhplus               = 20,
  theoretical_mhplus                = 21,
  delta_mhplus                      = 22,
  delta_ppm                         = 23,
  peptide_prophet_probability       = 24, ///< no PSI MS description
  peptide_inter_prophet_probability = 25, ///< no PSI MS description
  tandem_hyperscore                 = 26, ///< X!Tandem hyperscore MS:1001331
  mascot_score = 27, ///< PSI-MS MS:1001171 mascot:score 56.16
  mascot_expectation_value =
    28, ///< PSI-MS MS:1001172 mascot:expectation value 2.42102904673618e-006
  omssa_evalue        = 29, ///< MS:1001328  "OMSSA E-value." [PSI:PI]
  omssa_pvalue        = 30, ///< MS:1001329  "OMSSA p-value." [PSI:PI]
  msgfplus_raw        = 31, ///< MS:1002049  "MS-GF raw score." [PSI:PI]
  msgfplus_denovo     = 32, ///< MS:1002050  "MS-GF de novo score." [PSI:PI]
  msgfplus_energy     = 33, ///< MS:1002051  "MS-GF energy score." [PSI:PI]
  msgfplus_SpecEValue = 34, ///< MS:1002052  "MS-GF spectral E-value." [PSI:PI]
  msgfplus_EValue     = 35, ///< MS:1002053  "MS-GF E-value." [PSI:PI]
  msgfplus_isotope_error = 36, ///< MS:1002053  "MS-GF E-value." [PSI:PI]
  comet_xcorr   = 37, ///< MS:1002252  "The Comet result 'XCorr'." [PSI:PI]
  comet_deltacn = 38, ///< MS:1002253  "The Comet result 'DeltaCn'." [PSI:PI]
  comet_deltacnstar =
    39, ///< MS:1002254  "The Comet result 'DeltaCnStar'." [PSI:PI]
  comet_spscore = 40, ///< MS:1002255  "The Comet result 'SpScore'." [PSI:PI]
  comet_sprank  = 41, ///< MS:1002256  "The Comet result 'SpRank'." [PSI:PI]
  comet_expectation_value =
    42, ///< MS:1002257  "The Comet result 'Expectation value'." [PSI:PI]
};

class PeptideListWindow;

class PeptideTableModel : public QAbstractTableModel
{
  Q_OBJECT
  public:
  PeptideTableModel(PeptideListWindow *parent);
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant
  headerData(int section, Qt::Orientation orientation, int role) const override;
  QVariant data(const QModelIndex &index,
                int role = Qt::DisplayRole) const override;

  static const QString getTitle(PeptideListColumn column);
  static const QString getDescription(PeptideListColumn column);
  static PeptideListColumn getPeptideListColumn(std::int8_t column);
  bool hasColumn(PeptideListColumn column);

  void setProteinMatch(ProteinMatch *p_protein_match);
  ProteinMatch *getProteinMatch();
  signals:
  void peptideEvidenceClicked(PeptideEvidence *p_peptide_evidence);

  public slots:
  void onPeptideDataChanged();

  private:
  static const QString getTitle(std::int8_t column);
  static const QString getDescription(std::int8_t column);
  static int getColumnWidth(int column);

  private:
  ProteinMatch *_p_protein_match            = nullptr;
  PeptideListWindow *_p_peptide_list_window = nullptr;
  // contains columns to display (present in this peptide match)
  std::set<PeptideListColumn> _engine_columns_to_display;
};
