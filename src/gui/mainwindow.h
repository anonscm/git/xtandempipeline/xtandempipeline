
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once

#include <QMainWindow>
#include <QFileInfo>
#include <QCloseEvent>
#include <pappsomspp/types.h>
#include <pappsomspp/peptide/peptide.h>
#include <pappsomspp/massspectrum/massspectrum.h>
#include "core/project.h"
#include "project_view/projectwindow.h"
#include "load_results_dialog/loadresultsdialog.h"
#include "export/export_spreadsheet_dialog/exportspreadsheetdialog.h"
#include "waiting_message_dialog/waitingmessagedialog.h"
#include "about_dialog/aboutdialog.h"
#include "tandem_run_dialog/tandemrundialog.h"
#include "core/tandem_run/tandemrunbatch.h"
#include "utils/httpversion.h"
#include "gui/edit/edit_settings/editsettings.h"
#include "gui/export/export_masschroq_dialog/exportmasschroqdialog.h"
#include "workerthread.h"


namespace Ui
{
class Main;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT
  // friend class WorkerThread;

  public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

  /** @brief tells if the worker should stop running his job
   * @param bool true asks to stop
   */
  bool stopWorkerThread();
  WorkerThread *getWorkerThread();

  public slots:
  void run();
  void loadResults();
  void selectXpipFile();
  void doActionAbout();
  void doActionQuit();
  void doActionFasta();
  void doActionFastaOneBySubgroup();
  void doActionFastaOneByGroup();
  void doActionMassChroQ();
  void doActionMassChroqPRM();
  void doActionProticDb();
  void doActionLabelingMethods();
  void doActionTandemRun();
  void doActionSettings();
  void doActionSpreadsheet();
  void doActionModifications();
  void doActionSaveProject();
  void doDisplayLoadingMessage(QString message);
  void doDisplayLoadingMessagePercent(QString message, int value);
  void doDisplayJobFinished(QString message);
  void doDisplayJobCanceled(QString message);
  void doWorkerAppendText(const char *);
  void doWorkerSetText(QString);
  void doProjectReady(ProjectSp project_sp);
  void doLoadingResultsReady(ProjectSp project_sp);
  void doProjectNotReady(QString error);
  void doAcceptedLoadResultDialog();
  void doAcceptedTandemRunDialog();
  void doAcceptedExportSpreadsheetDialog();
  void doAcceptedExportMasschroqDialog();
  void doOperationFailed(QString);
  void doOperationFinished();
  void doGroupingFinished();
  void doActionSpectralCountingMcq();
  void doCheckNewVersion();
  // void peptideEdited(QString peptideStr);
  // void setColor(const QColor &color);
  // void setShape(Shape shape);
  signals:
  // void peptideChanged(pappso::PeptideSp peptide);
  void operateXpipFile(QString xpip_file);
  void operateLoadingResults(bool is_individual,
                             AutomaticFilterParameters param,
                             QStringList file_list);
  void operateWritingXpipFile(QString filename, ProjectSp project_sp);
  void
  operateWritingOdsFile(QString filename, QString format, ProjectSp project_sp);
  void operateWritingMassChroqFile(QString filename,
                                   ProjectSp project_sp,
                                   MasschroqFileParameters params);
  void operateWritingMassChroqPrmFile(QString filename, ProjectSp project_sp);
  void operateWritingProticFile(QString filename, ProjectSp project_sp);
  void operateWritingMcqrSpectralCountFile(QString filename,
                                           ProjectSp project_sp);
  void operateWritingFastaFile(QString filename,
                               ProjectSp project_sp,
                               ExportFastaType type);
  void operateGrouping(ProjectSp project_sp);
  void operateRunningXtandem(TandemRunBatch tandem_run_batch);
  void projectNameChanged(QString project_name);

  protected:
  void closeEvent(QCloseEvent *event) override;
  void setProjectSp(ProjectSp project_sp);
  /** @brief centralized way to modify the project name
   * if this windows contains a project, then it will modify its name
   * this will emit a signal to provide the new name to any widget
   */
  void showProjectName();


  public:
  void showWaitingMessage(const QString title);
  void hideWaitingMessage();
  void viewError(QString error);

  private:
  int chooseSaveMssgBox();

  private:
  QCoreApplication *_p_app;
  Ui::Main *ui;
  QThread _worker_thread;
  WorkerThread *mp_worker;

  ProjectSp _project_sp = nullptr;

  ProjectWindow *_project_window                        = nullptr;
  LoadResultsDialog *_p_load_results_dialog             = nullptr;
  ExportSpreadsheetDialog *_p_export_spreadsheet_dialog = nullptr;
  ExportMasschroqDialog *_p_export_masschroq_dialog     = nullptr;
  WaitingMessageDialog *_p_waiting_message_dialog;
  AboutDialog *_p_about_dialog          = nullptr;
  TandemRunDialog *_p_tandem_run_dialog = nullptr;
  EditSettings *_p_edit_settings        = nullptr;

  HttpVersion m_onlineVersion;
};
