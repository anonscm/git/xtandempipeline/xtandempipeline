
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QMainWindow>
#include <QAction>
#include <QMenu>
#include "proteintablemodel.h"
#include "proteintableproxymodel.h"

class ProjectWindow;

// http://doc.qt.io/qt-4.8/qt-itemviews-chart-mainwindow-cpp.html
namespace Ui
{
class ProteinView;
}

class ProteinListWindow;
class ProteinListQactionColumn : public QAction
{
  Q_OBJECT
  public:
  explicit ProteinListQactionColumn(ProteinListWindow *parent,
                                    ProteinListColumn column);
  ~ProteinListQactionColumn();

  public slots:
  void doToggled(bool toggled);

  private:
  ProteinListWindow *_p_protein_list_window;
  ProteinListColumn _column;
};

class ProteinListWindow : public QMainWindow
{
  Q_OBJECT

  friend class ProteinTableModel;
  friend class ProteinTableProxyModel;

  public:
  explicit ProteinListWindow(ProjectWindow *parent = 0);
  ~ProteinListWindow();
  void setIdentificationGroup(IdentificationGroup *p_identification_group);
  void setProteinListColumnDisplay(ProteinListColumn column, bool toggled);
  bool getProteinListColumnDisplay(ProteinListColumn column) const;
  void resizeColumnsToContents();

  void edited();
  virtual void closeEvent(QCloseEvent *event) override;

  public slots:
  void doFocusReceived(bool has_focus);
  void
  doIdentificationGroupGrouped(IdentificationGroup *p_identification_group);
  void doExportAsOdsFile();
  void doProjectNameChanged(QString name);
  // void peptideEdited(QString peptideStr);
  // void setColor(const QColor &color);
  // void setShape(Shape shape);
  signals:
  void identificationGroupEdited(IdentificationGroup *p_identification_group);
  void proteinDataChanged();
  void projectStatusChanged();

  protected slots:
  void doNotValidHide(bool hide);
  void doNotCheckedHide(bool hide);
  void doNotGroupedHide(bool hide);
  void doMsrunFileSearch(QString msr_run_file_search);
  void doModificationSearch(QString mod_search);
  void doScanNumberSearch(int scan_num);
  void doProxyLayoutChanged();
  void showContextMenu(const QPoint &);
  void updateStatusBar();
  void onProteinSearchEdit(QString protein_search_string);
  void doSearchOn(QString search_on);

  protected:
  void askProteinDetailView(ProteinMatch *p_protein_match);
  void askPeptideListView(ProteinMatch *p_protein_match);
  ProjectWindow *getProjectWindow();


  private:
  IdentificationGroup *_p_identification_group = nullptr;
  Ui::ProteinView *ui;
  ProteinTableModel *_protein_table_model_p = nullptr;
  ProteinTableProxyModel *_p_proxy_model    = nullptr;
  ProjectWindow *_project_window;
  QMenu *_p_context_menu  = nullptr;
  bool _display_evalue    = true;
  bool _display_accession = true;
};
