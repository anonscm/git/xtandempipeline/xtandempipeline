
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "../../../core/automaticfilterparameters.h"
#include <QWidget>


namespace Ui
{
class AutomaticFilterWidget;
}

class AutomaticFilterWidget : public QWidget
{
  Q_OBJECT

  public:
  explicit AutomaticFilterWidget(QWidget *parent);
  ~AutomaticFilterWidget();

  void setAutomaticFilterParameters(const AutomaticFilterParameters &params);
  AutomaticFilterParameters getAutomaticFilterParameters() const;
  int getAutomaticFilterWindowWidth() const;

  public slots:

  signals:
  void automaticFilterParametersChanged(AutomaticFilterParameters parameters);

  private slots:
  void doPeptideEvalue(double evalue);
  void doProteinEvalue(double evalue);
  void doProteinLogEvalue(double evalue);
  void doPeptideNumber(int number);
  void doPepreproChanged(int number);
  void doCrossSample(bool is_cross_sample);
  void doSetParameters();
  void doValueChanged(double value);

  private:
  Ui::AutomaticFilterWidget *ui;
  bool _signal       = true;
  bool _emit_changed = true;

  AutomaticFilterParameters _parameters;
};
