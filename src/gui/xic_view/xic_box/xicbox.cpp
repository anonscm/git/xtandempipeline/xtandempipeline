/**
 * \file src/gui/xic_view/xic_box/xicbox.cpp
 * \date 12/1/2018
 * \author Olivier Langella
 * \brief XIC box widget
 */
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "xicbox.h"
#include "ui_xic_box.h"
#include <QMessageBox>
#include <pappsomspp/exception/exceptionnotfound.h>
#include "../xicworkerthread.h"
#include "../../project_view/projectwindow.h"
#include <pappsomspp/processing/detection/tracedetectionzivy.h>
#include <pappsomspp/exception/exceptionnotpossible.h>


bool
XicBoxNaturalIsotope::contains(const pappso::TracePeakCstSPtr &peak) const
{
  for(const pappso::TracePeakCstSPtr &peak_i : detected_peak_list)
    {
      if(peak_i.get() == peak.get())
        {
          return true;
        }
    }
  return false;
}

class XicDetectionList : public pappso::TraceDetectionSinkInterface
{
  public:
  XicDetectionList()
  {
    _max_peak = nullptr;
  }
  void
  setPeptideEvidenceList(
    const std::vector<const PeptideEvidence *> &peptide_evidence_list)
  {
    for(const PeptideEvidence *p_evidence : peptide_evidence_list)
      {
        _rt_list.push_back(p_evidence->getRetentionTime());
      }
  }
  void
  setTracePeak(pappso::TracePeak &xic_peak) override
  {
    pappso::TracePeakCstSPtr peak_sp = xic_peak.makeTracePeakCstSPtr();
    _peak_list.push_back(peak_sp);

    for(pappso::pappso_double rt : _rt_list)
      {
        if(xic_peak.containsRt(rt))
          {
            if((_max_peak == nullptr) ||
               (xic_peak.getArea() > _max_peak.get()->getArea()))
              {
                _max_peak = peak_sp;
              }
          }
      }
  };
  const pappso::TracePeakCstSPtr &
  getMatchedPeak() const
  {
    return _max_peak;
  }
  const std::vector<pappso::TracePeakCstSPtr> &
  getXicPeakList() const
  {
    return _peak_list;
  };
  void
  clear()
  {
    _peak_list.clear();
    _max_peak = nullptr;
  };

  private:
  unsigned int _count = 0;
  std::vector<pappso::TracePeakCstSPtr> _peak_list;
  std::vector<pappso::pappso_double> _rt_list;
  pappso::TracePeakCstSPtr _max_peak;
};


XicBox::XicBox(XicWindow *parent) : QWidget(parent), ui(new Ui::XicBox)
{
  _p_xic_window = parent;
  ui->setupUi(this);

  XicWorkerThread *p_worker = new XicWorkerThread(this);
  p_worker->moveToThread(&_xic_thread);
  _xic_thread.start();


  // ui->histo_widget->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom );
  // ui->xic_list_widget->setLayout(new QVBoxLayout(ui->xic_list_widget));
  ui->histo_widget->legend->setVisible(true);

  if(_p_xic_window->isRetentionTimeSeconds())
    {
      ui->xic_widget->setRetentionTimeInSeconds();
    }
  else
    {
      ui->xic_widget->setRetentionTimeInMinutes();
    }

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(this, &XicBox::loadXic, p_worker, &XicWorkerThread::doXicLoad);
  connect(p_worker, &XicWorkerThread::xicLoaded, this, &XicBox::setXic);
  connect(p_worker, &XicWorkerThread::operationFailed, this, &XicBox::error);


  connect(this,
          &XicBox::computeIsotopeMassList,
          p_worker,
          &XicWorkerThread::doComputeIsotopeMassList);
  connect(p_worker,
          &XicWorkerThread::isotopeMassListComputed,
          this,
          &XicBox::setIsotopeMassList);

  connect(
    _p_xic_window, &XicWindow::reExtractXicNeeded, this, &XicBox::reExtractXic);
  /*  connect(ui->xic_widget,
          qOverload<std::vector<std::pair<pappso::XicCstSPtr,
  pappso::XicPeakSp>>>( &pappso::XicWidget::xicPeakListChanged), this,
          &XicBox::setXicPeakList);

  connect(ui->xic_widget,
          &pappso::XicWidget::clicked,
          this,
          &XicBox::onXicWidgetClick);
*/
  connect(_p_xic_window,
          &XicWindow::rtUnitChangeNeeded,
          this,
          &XicBox::onRtUnitChanged);
#else
// Qt4 code
#endif


  QStringList msrun_list;
  for(MsRunSp msrun_sp : _p_xic_window->getProjectWindow()
                           ->getProjectP()
                           ->getMsRunStore()
                           .getMsRunList())
    {
      msrun_list << msrun_sp.get()->getFileName();
      qDebug() << "ProteinListWindow::setIdentificationGroup "
               << msrun_sp.get()->getFileName();
    }
}

XicBox::~XicBox()
{
  qDebug() << "XicBox::~XicBox";
  _xic_thread.quit();
  _xic_thread.wait();
}

void
XicBox::onXicWidgetClick(double rt, double intensity)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  ui->xic_widget->clearXicPeakBorders();
  std::vector<pappso::TracePeakCstSPtr> draw_peak_borders;
  for(XicBoxNaturalIsotope peak : _natural_isotope_list)
    {
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
      draw_peak_borders.push_back(peak.one_peak_sp);
      if(peak.one_peak_sp.get() != nullptr)
        {
          ui->xic_widget->drawXicPeakBorders(peak.one_peak_sp);
        }
    }

  if(_peptide_evidence_list.size() == 0)
    {
      drawObservedAreaBars(draw_peak_borders);
    }
}

void
XicBox::remove()
{
  _p_xic_window->removeXicBox(this);
}

void
XicBox::error(QString error_message)
{
  QMessageBox::warning(this, tr("Error extracting XIC :"), error_message);
}

void
XicBox::extractXicInOtherMsRun()
{
  qDebug() << "XicBox::extractXicInOtherMsRun begin";
  try
    {
      QString location =
        QFileInfo(_p_peptide_evidence->getMsRunP()->getFileName())
          .absoluteDir()
          .absolutePath();
      QStringList filenames = QFileDialog::getOpenFileNames(
        this, tr("Open new mzXML"), location, tr("mzXML Files (*.mzXML)"));

      foreach(QString new_filename, filenames)
        {
          MsRunSp msrun_sp = _p_xic_window->getProjectWindow()
                               ->getProjectP()
                               ->getMsRunStore()
                               .getInstance(new_filename);
          _p_xic_window->addXicInMsRun(_p_peptide_evidence, msrun_sp);
        }
    }
  catch(pappso::ExceptionNotFound &error)
    {
      qDebug() << "XicBox::extractXicInOtherMsRun not found ";
    }
}

void
XicBox::setPeptideEvidence(const PeptideEvidence *p_peptide_evidence)
{


  _p_peptide_evidence = p_peptide_evidence;
  _msrun_sp = p_peptide_evidence->getIdentificationDataSource()->getMsRunSp();

  ui->peptide_label->setText(
    _p_peptide_evidence->getPeptideXtpSp().get()->toString());
  ui->charge_label->setText(
    QString("%1").arg(_p_peptide_evidence->getCharge()));
  ui->mz_label->setText(
    QString::number(_p_peptide_evidence->getTheoreticalMz(), 'f', 4));
  ui->msrun_label->setText(
    QFileInfo(_p_peptide_evidence->getMsRunP()->getFileName()).fileName());
  ui->msrun_label->setToolTip("Test2");
  // get same xic peptide evidence (msrun, peptide, charge)
  // p_projet
  _peptide_evidence_list.clear();
  _p_xic_window->getProjectWindow()
    ->getProjectP()
    ->getSameXicPeptideEvidenceList(
      _peptide_evidence_list,
      _msrun_sp.get(),
      _p_peptide_evidence->getPeptideXtpSp().get(),
      _p_peptide_evidence->getCharge());

  emit computeIsotopeMassList(_p_peptide_evidence->getPeptideXtpSp(),
                              _p_peptide_evidence->getCharge(),
                              _p_xic_window->getXicExtractPrecision(),
                              ui->isotopic_distribution_spinbox->value());
}

void
XicBox::setPeptideEvidenceInMsRun(const PeptideEvidence *p_peptide_evidence,
                                  MsRunSp msrun_sp)
{
  _p_peptide_evidence = p_peptide_evidence;
  _msrun_sp           = msrun_sp;

  ui->peptide_label->setText(
    _p_peptide_evidence->getPeptideXtpSp().get()->toString());
  ui->charge_label->setText(
    QString("%1").arg(_p_peptide_evidence->getCharge()));
  ui->mz_label->setText(
    QString::number(_p_peptide_evidence->getTheoreticalMz(), 'f', 4));
  ui->msrun_label->setText(QFileInfo(msrun_sp->getFileName()).fileName());
  ui->msrun_label->setToolTip("Test");
  // get same xic peptide evidence (msrun, peptide, charge)
  // p_projet
  _peptide_evidence_list.clear();

  _p_xic_window->getProjectWindow()
    ->getProjectP()
    ->getSameXicPeptideEvidenceList(
      _peptide_evidence_list,
      _msrun_sp.get(),
      _p_peptide_evidence->getPeptideXtpSp().get(),
      _p_peptide_evidence->getCharge());
  //_p_xic_window->getProjectWindow()->getProjectP()->getSameXicPeptideEvidenceList(p_peptide_evidence,
  //_peptide_evidence_list);

  emit computeIsotopeMassList(_p_peptide_evidence->getPeptideXtpSp(),
                              _p_peptide_evidence->getCharge(),
                              _p_xic_window->getXicExtractPrecision(),
                              ui->isotopic_distribution_spinbox->value());
}

void
XicBox::setXic(std::vector<pappso::XicCstSPtr> xic_sp_list)
{
  qDebug() << "XicBox::setXic begin " << xic_sp_list.size();
  ui->xic_widget->clear();
  for(int i = 0; i < xic_sp_list.size(); i++)
    {
      _natural_isotope_list[i].xic_sp = xic_sp_list[i];
    }
  // pappso::XicWidget * xic_widget = new pappso::XicWidget(this);
  // ui->xic_list_widget->layout()->addWidget(xic_widget);
  for(XicBoxNaturalIsotope &xic_isotope : _natural_isotope_list)
    {
      // qDebug() << "XicBox::setXic xic " << xic_isotope.xic_sp.get();
      if(xic_isotope.xic_sp.get() == nullptr)
        {
          throw new pappso::PappsoException(
            "Error in XicBox::setXic:\n xic_isotope.xic_sp.get() == nullptr");
        }
      qDebug() << "XicBox::setXic xic_isotope.xic_sp.size() "
               << xic_isotope.xic_sp.get()->size();
      ui->xic_widget->addXicSp(xic_isotope.xic_sp);
      QString isotope_name = QString("+%1").arg(
        xic_isotope.peptide_natural_isotope_sp.get()->getIsotopeNumber());
      if(xic_isotope.peptide_natural_isotope_sp.get()->getIsotopeRank() > 1)
        {
          isotope_name =
            QString("+%1 [%2]")
              .arg(xic_isotope.peptide_natural_isotope_sp.get()
                     ->getIsotopeNumber())
              .arg(
                xic_isotope.peptide_natural_isotope_sp.get()->getIsotopeRank());
        }
      isotope_name.append(QString(" (%1%)").arg((
        int)(xic_isotope.peptide_natural_isotope_sp.get()->getIntensityRatio() *
             100)));
      ui->xic_widget->setName(xic_isotope.xic_sp.get(), isotope_name);

      if(xic_isotope.peptide_natural_isotope_sp.get()->getIsotopeNumber() == 0)
        {
          for(const PeptideEvidence *peptide_evidence : _peptide_evidence_list)
            {
              qDebug() << "XicBox::setXic peptide_evidence "
                       << peptide_evidence->getRetentionTime();
              ui->xic_widget->addMsMsEvent(
                xic_isotope.xic_sp.get(), peptide_evidence->getRetentionTime());
            }
        }
    }
  qDebug() << "XicBox::setXic rescale";

  if(!_scaled)
    {
      ui->xic_widget->rescale();
      _scaled = true;
    }

  /*
  if (_isotope_mass_list.size() > _xic_widget_list.size()) {
      emit loadXic(_p_peptide_evidence->getMsRunP(),
  _isotope_mass_list[_xic_widget_list.size()].get()->getMz(),
  _p_xic_window->getXicExtractPrecision(), XicExtractMethod::max);
  }
  */
  XicDetectionList xic_list;
  xic_list.setPeptideEvidenceList(_peptide_evidence_list);

  std::vector<pappso::TracePeakCstSPtr> draw_peak_borders;

  for(XicBoxNaturalIsotope &xic_isotope : _natural_isotope_list)
    {
      try
        {
          xic_list.clear();
          _p_xic_window->xicDetect(*(xic_isotope.xic_sp.get()), &xic_list);
          xic_isotope.matched_peak_sp    = xic_list.getMatchedPeak();
          xic_isotope.detected_peak_list = xic_list.getXicPeakList();
          ui->xic_widget->addXicPeakList(xic_isotope.xic_sp.get(),
                                         xic_list.getXicPeakList());
          draw_peak_borders.push_back(xic_isotope.matched_peak_sp);
        }
      catch(const pappso::ExceptionNotPossible &e)
        {
          qDebug() << e.qwhat();
        }
    }

  qDebug() << "XicBox::setXic plot";
  ui->xic_widget->plot();
  drawObservedAreaBars(draw_peak_borders);
}


void
XicBox::drawObservedAreaBars(
  const std::vector<pappso::TracePeakCstSPtr> &observed_peak_to_draw_list)
{
  if(_isotope_ratio_graph_observed_intensity == nullptr)
    {
      qDebug() << "XicBox::setXic plot new QCPBars";
      _isotope_ratio_graph_observed_intensity =
        new QCPBars(ui->histo_widget->xAxis, ui->histo_widget->yAxis2);
    }
  else
    {
      qDebug() << "XicBox::setXic plot clearData";
      _isotope_ratio_graph_observed_intensity->data().clear();
    }
  _isotope_ratio_graph_observed_intensity->setName("peak area");
  // ui->histo_widget->yAxis2->setLabel("intensity");


  QVector<double> observed_intensity_data;
  QVector<QString> labels;

  QVector<double> ticks;
  double sum = 0;
  int i      = 0;

  qDebug() << "XicBox::setXic plot _isotope_mass_list";
  for(const XicBoxNaturalIsotope &xic_isotope : _natural_isotope_list)
    {
      pappso::TracePeakCstSPtr peak_to_draw;
      for(pappso::TracePeakCstSPtr observed_peak : observed_peak_to_draw_list)
        {
          if(xic_isotope.contains(observed_peak))
            {
              peak_to_draw = observed_peak;
            }
        }
      if(peak_to_draw.get() == nullptr)
        {
        }
      else
        {
          sum += peak_to_draw.get()->getArea();
          observed_intensity_data << peak_to_draw.get()->getArea();
          QString isotope_name = QString("+%1").arg(
            xic_isotope.peptide_natural_isotope_sp.get()->getIsotopeNumber());
          if(xic_isotope.peptide_natural_isotope_sp.get()->getIsotopeRank() > 1)
            {
              isotope_name =
                QString("+%1 [%2]")
                  .arg(xic_isotope.peptide_natural_isotope_sp.get()
                         ->getIsotopeNumber())
                  .arg(xic_isotope.peptide_natural_isotope_sp.get()
                         ->getIsotopeRank());
            }
          isotope_name.append(QString(" (%1%)").arg(
            (int)(xic_isotope.peptide_natural_isotope_sp.get()
                    ->getIntensityRatio() *
                  100)));
          labels << isotope_name;
          ticks << i;
        }
      i++;
    }

  _isotope_ratio_graph_observed_intensity->setPen(QPen(QColor("red")));
  //_graph_peak_surface_list.back()->setScatterStyle(QCPScatterStyle::ssDot);
  // observed_intensity->setBrush(QBrush(QColor(170, 255, 0, 0)));

  if(ticks.size() > 0)
    {
      _isotope_ratio_graph_observed_intensity->setData(ticks,
                                                       observed_intensity_data);

      ui->histo_widget->yAxis2->setVisible(true);
      ui->histo_widget->yAxis2->setRange(0, sum);
      ui->histo_widget->replot();
    }
  qDebug() << "XicBox::setXic end";
}


void
XicBox::setIsotopeMassList(
  std::vector<pappso::PeptideNaturalIsotopeAverageSp> isotope_mass_list)
{
  _natural_isotope_list.clear();
  std::vector<pappso::pappso_double> mass_list;
  for(pappso::PeptideNaturalIsotopeAverageSp &natural_isotope_average :
      isotope_mass_list)
    {
      mass_list.push_back(natural_isotope_average.get()->getMz());
      _natural_isotope_list.push_back(
        {nullptr, natural_isotope_average, nullptr, nullptr});
    }

  emit loadXic(_msrun_sp,
               mass_list,
               _p_xic_window->getXicExtractPrecision(),
               pappso::XicExtractMethod::max);

  // histogram
  if(m_theoretical_ratio == nullptr)
    {
      qDebug() << "ZZZZZZZZZZZZZ";
      m_theoretical_ratio =
        new QCPBars(ui->histo_widget->xAxis, ui->histo_widget->yAxis);
    }
  else
    {
      qDebug() << "EEEEEEEEEEE";
      m_theoretical_ratio->data()->clear();
    }
  m_theoretical_ratio->setName("th. ratio");
  ui->histo_widget->xAxis->setLabel("isotopes");
  // ui->histo_widget->yAxis->setLabel("th. ratio");

  QVector<double> theoretical_ratio_data;

  QVector<double> ticks;
  QVector<QString> labels;
  int i      = 0;
  double sum = 0;
  for(const XicBoxNaturalIsotope &xic_isotope : _natural_isotope_list)
    {
      sum += xic_isotope.peptide_natural_isotope_sp.get()->getIntensityRatio();
      theoretical_ratio_data
        << xic_isotope.peptide_natural_isotope_sp.get()->getIntensityRatio();
      QString isotope_name = QString("+%1").arg(
        xic_isotope.peptide_natural_isotope_sp.get()->getIsotopeNumber());
      if(xic_isotope.peptide_natural_isotope_sp.get()->getIsotopeRank() > 1)
        {
          isotope_name =
            QString("+%1 [%2]")
              .arg(xic_isotope.peptide_natural_isotope_sp.get()
                     ->getIsotopeNumber())
              .arg(
                xic_isotope.peptide_natural_isotope_sp.get()->getIsotopeRank());
        }
      isotope_name.append(QString(" (%1%)").arg((
        int)(xic_isotope.peptide_natural_isotope_sp.get()->getIntensityRatio() *
             100)));
      labels << isotope_name;
      ticks << i;
      i++;
    }
  // QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
  // textTicker->addTicks(ticks, labels);
  // ui->histo_widget->xAxis->setTicker(textTicker);
  ui->histo_widget->xAxis->setTickLabelRotation(60);
  // ui->histo_widget->xAxis->setSubTicks(false);
  // ui->histo_widget->xAxis->setTickLength(0, 4);
  // ui->histo_widget->xAxis->setRange(0, 8);
  /*
    ui->histo_widget->xAxis->setAutoTickStep(
      false); // <-- disable to use your own value

    ui->histo_widget->xAxis->setTickStep(1);
  */

  m_theoretical_ratio->setData(ticks, theoretical_ratio_data);


  ui->histo_widget->yAxis->setRange(0, sum);
  ui->histo_widget->xAxis->setRange(-0.8, _natural_isotope_list.size());
  ui->histo_widget->replot();
}


void
XicBox::reExtractXic()
{
  qDebug() << "XicBox::reExtractXic begin";
  ui->xic_widget->clear();

  std::vector<pappso::pappso_double> mass_list;
  for(const XicBoxNaturalIsotope &xic_isotope : _natural_isotope_list)
    {
      mass_list.push_back(
        xic_isotope.peptide_natural_isotope_sp.get()->getMz());
    }
  emit loadXic(_msrun_sp,
               mass_list,
               _p_xic_window->getXicExtractPrecision(),
               _p_xic_window->getXicExtractionMethod());
  qDebug() << "XicBox::reExtractXic end";
}

void
XicBox::setRetentionTime(double rt)
{
  qDebug() << "XicBox::setRetentionTime begin";
  ui->rt_label->setText(tr("rt=%1 (sec) rt=%2 (min)").arg(rt).arg(rt / 60));
}
void
XicBox::setXicPeakList(
  std::vector<std::pair<pappso::XicCstSPtr, pappso::TracePeakCstSPtr>>
    xic_peak_list)
{
  qDebug() << "XicBox::setXicPeakList begin";
  QString html;
  for(XicBoxNaturalIsotope &xic_isotope : _natural_isotope_list)
    {
      xic_isotope.one_peak_sp = nullptr;
    }

  for(std::pair<pappso::XicCstSPtr, pappso::TracePeakCstSPtr> &pair_xic_peak :
      xic_peak_list)
    {
      for(XicBoxNaturalIsotope &xic_isotope : _natural_isotope_list)
        {
          if(xic_isotope.xic_sp.get() == pair_xic_peak.first.get())
            {
              xic_isotope.one_peak_sp = pair_xic_peak.second;
            }
        }
      html.append(tr("<p>%1<br/>area: %2<br/>rt begin: %3<br/>rt max: "
                     "%4<br/>rt end: %5<br/></p>")
                    .arg(ui->xic_widget->getName(pair_xic_peak.first.get()))
                    .arg(pair_xic_peak.second.get()->getArea())
                    .arg(pair_xic_peak.second.get()->getLeftBoundary().x)
                    .arg(pair_xic_peak.second.get()->getMaxXicElement().x)
                    .arg(pair_xic_peak.second.get()->getRightBoundary().x));
    }

  ui->xic_widget->setToolTip(html);
  /*
      if (_popup_peak_info == nullptr) delete _popup_peak_info;
      _popup_peak_info = new QFrame( this ,Qt::Popup);
      //_popup_peak_info->setFrameStyle(Qt::WinPanel|Qt::Raised );
      _popup_peak_info->resize(150,100);
      _popup_peak_info->show();
      */
}


void
XicBox::onRtUnitChanged()
{
  if(_p_xic_window->isRetentionTimeSeconds())
    {
      ui->xic_widget->setRetentionTimeInSeconds();
    }
  else
    {
      ui->xic_widget->setRetentionTimeInMinutes();
    }
}

void
XicBox::onIsotopicDistributionClick()
{
  //   ui->histo_widget->legend->clear();
  setPeptideEvidence(_p_peptide_evidence);
}
