/**
 * \file src/gui/xic_view/xicwindow.cpp
 * \date 11/1/2018
 * \author Olivier Langella
 * \brief XIC window
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "xicwindow.h"
#include "ui_xic_window.h"
#include "../project_view/projectwindow.h"
#include "xic_box/xicbox.h"

XicWindow::XicWindow(ProjectWindow *parent)
  : QMainWindow(parent), ui(new Ui::XicWindow)
{
  _project_window = parent;

  ui->setupUi(this);

  QSettings settings;
  QString precision_str = settings.value("xic/precision", "10 ppm").toString();

  ui->xic_precision->setPrecision(
    pappso::PrecisionFactory::fromString(precision_str));

  ZivyParams zivy_params;
  zivy_params.loadSettings();


  msp_detect_zivy = zivy_params.newTraceDetectionZivySPtr();

  _p_zivy_dialog = new ZivyDialog(this);
  _p_zivy_dialog->setZivyParams(zivy_params);


  connect(_p_zivy_dialog,
          &ZivyDialog::accepted,
          this,
          &XicWindow::doAcceptedZivyDialog);

#if QT_VERSION >= 0x050000
  // Qt5 code

#else
// Qt4 code
#endif
}

XicWindow::~XicWindow()
{
  qDebug() << "XicWindow::~XicWindow";
  delete _p_zivy_dialog;
}

void
XicWindow::doEditZivyParams()
{
  _p_zivy_dialog->show();
}

void
XicWindow::clear()
{
  QLayoutItem *wItem;
  while(wItem = ui->xic_vertical_layout->layout()->takeAt(0))
    {
      wItem->widget()->setVisible(false);
      ui->xic_vertical_layout->layout()->removeWidget(wItem->widget());
      delete wItem->widget();
    }
}

ProjectWindow *
XicWindow::getProjectWindow()
{
  return _project_window;
}
void
XicWindow::addXic(const PeptideEvidence *p_peptide_evidence)
{
  XicBox *p_box = new XicBox(this);
  p_box->setPeptideEvidence(p_peptide_evidence);
  ui->xic_vertical_layout->layout()->addWidget(p_box);
}

void
XicWindow::addXicInMsRun(const PeptideEvidence *p_peptide_evidence,
                         MsRunSp msrun_sp)
{
  XicBox *p_box = new XicBox(this);
  p_box->setPeptideEvidenceInMsRun(p_peptide_evidence, msrun_sp);
  ui->xic_vertical_layout->layout()->addWidget(p_box);
}

void
XicWindow::removeXicBox(XicBox *xic_box)
{
  xic_box->setVisible(false);
  ui->xic_vertical_layout->layout()->removeWidget(xic_box);
  delete xic_box;
}

pappso::PrecisionPtr
XicWindow::getXicExtractPrecision() const
{
  return (ui->xic_precision->getPrecision());
}


pappso::XicExtractMethod
XicWindow::getXicExtractionMethod() const
{
  return (ui->xic_extraction_method_widget->getXicExtractionMethod());
}


void
XicWindow::xicPrecisionChanged(pappso::PrecisionPtr precision)
{
  qDebug() << "XicWindow::xicPrecisionChanged begin";
  qDebug() << "XicWindow::xicPrecisionChanged emit";
  emit reExtractXicNeeded();
  qDebug() << "XicWindow::xicPrecisionChanged end";
}

void
XicWindow::xicDetect(const pappso::Xic &xic,
                     pappso::TraceDetectionSinkInterface *sink) const
{
  msp_detect_zivy->detect(xic, *sink);
}

void
XicWindow::rtUnitChanged(QAbstractButton *button)
{
  qDebug() << "XicWindow::rtUnitChanged begin";
  emit rtUnitChangeNeeded();
  qDebug() << "XicWindow::rtUnitChanged end";
}

bool
XicWindow::isRetentionTimeSeconds() const
{
  if(ui->rt_sec_radiobutton->isChecked())
    {
      return true;
    }
  return false;
}

void
XicWindow::doAcceptedZivyDialog()
{
  msp_detect_zivy = _p_zivy_dialog->getZivyParams().newTraceDetectionZivySPtr();
  _p_zivy_dialog->getZivyParams().saveSettings();
  emit reExtractXicNeeded();
}


void
XicWindow::doXicExtractionMethodChanged(pappso::XicExtractMethod xic_method)
{
  emit reExtractXicNeeded();
}
