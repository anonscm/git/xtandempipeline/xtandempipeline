/**
 * \file src/gui/xic_view/xicworkerthread.cpp
 * \date 12/1/2018
 * \author Olivier Langella
 * \brief XIC worker
 */
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include <tuple>
#include "xicworkerthread.h"
#include <pappsomspp/peptide/peptidenaturalisotopelist.h>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/exception/exceptionnotfound.h>
#include <QDebug>

XicWorkerThread::XicWorkerThread(XicBox *parent)
{
}
XicWorkerThread::~XicWorkerThread()
{
}
void
XicWorkerThread::doXicLoad(MsRunSp p_msrun,
                           std::vector<pappso::pappso_double> mz_list,
                           pappso::PrecisionPtr precision,
                           pappso::XicExtractMethod method)
{

  try
    {
      std::vector<pappso::MzRange> mass_list;
      for(pappso::pappso_double mass : mz_list)
        {
          mass_list.push_back(pappso::MzRange(mass, precision));
        }
      // std::vector<pappso::XicSp> xic_sp_list =
      // SpectrumStore::getXicSpFromMsRunSp(p_msrun.get(), mass_list, method);

      pappso::MsRunXicExtractorInterfaceSp extractor =
        p_msrun.get()->getMsRunXicExtractorInterfaceSp();
      if(extractor != nullptr)
        {
          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
          extractor.get()->setXicExtractMethod(method);


          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
          std::vector<pappso::XicCstSPtr> xic_sp_list =
            extractor.get()->getXicCstSPtrList(mass_list);

          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
          emit xicLoaded(xic_sp_list);
        }
    }
  catch(pappso::PappsoException &error)
    {
      emit operationFailed(tr("Error extracting XIC for MSrun %1:\n%2")
                             .arg(p_msrun->getFileName())
                             .arg(error.qwhat()));
    }
}


void
XicWorkerThread::doComputeIsotopeMassList(pappso::PeptideSp peptide_sp,
                                          unsigned int charge,
                                          pappso::PrecisionPtr precision,
                                          double minimum_isotope_pattern_ratio)
{
  qDebug() << "XicWorkerThread::doComputeIsotopeMassList "
           << precision->toString();
  std::vector<pappso::PeptideNaturalIsotopeAverageSp> isotope_mass_list;
  // compute isotope masses :
  if(peptide_sp != nullptr)
    {
      pappso::PeptideNaturalIsotopeList isotope_list(peptide_sp);
      isotope_mass_list = isotope_list.getByIntensityRatio(
        charge, precision, minimum_isotope_pattern_ratio);

      std::sort(isotope_mass_list.begin(),
                isotope_mass_list.end(),
                [](const pappso::PeptideNaturalIsotopeAverageSp &m,
                   const pappso::PeptideNaturalIsotopeAverageSp &n) -> bool {
                  unsigned int mn(m.get()->getIsotopeNumber()),
                    nn(n.get()->getIsotopeNumber());
                  unsigned int mr(m.get()->getIsotopeRank()),
                    nr(n.get()->getIsotopeRank());
                  return (std::tie(mn, mr) < std::tie(nn, nr));
                });
      emit isotopeMassListComputed(isotope_mass_list);
    }
}
