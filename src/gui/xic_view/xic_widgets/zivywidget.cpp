/**
 * \file src/gui/xic_view/xic_widgets/zivywidget.cpp
 * \date 29/5/2018
 * \author Olivier Langella
 * \brief Widget to edit Zivy quantification method
 */
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "zivywidget.h"
#include "ui_zivy_widget.h"
#include <QDebug>
#include <QSettings>
#include <memory>

std::shared_ptr<pappso::TraceDetectionInterface>
ZivyParams::newTraceDetectionZivySPtr() const
{
  return std::make_shared<pappso::TraceDetectionZivy>(_smoothing_half_window,
                                                      _minmax_half_window,
                                                      _maxmin_half_window,
                                                      _minmax_threshold,
                                                      _maxmin_threshold);
}

void
ZivyParams::saveSettings() const
{
  QSettings settings;
  settings.setValue("xic/zivy_maxmin_threshold",
                    QString("%1").arg(_maxmin_threshold));
  settings.setValue("xic/zivy_minmax_threshold",
                    QString("%1").arg(_minmax_threshold));
  settings.setValue("xic/zivy_maxmin_half_window",
                    QString("%1").arg(_maxmin_half_window));
  settings.setValue("xic/zivy_minmax_half_window",
                    QString("%1").arg(_minmax_half_window));
  settings.setValue("xic/zivy_smoothing",
                    QString("%1").arg(_smoothing_half_window));
}
void
ZivyParams::loadSettings()
{
  QSettings settings;
  _maxmin_threshold =
    settings
      .value("xic/zivy_maxmin_threshold", QString("%1").arg(_maxmin_threshold))
      .toDouble();
  _minmax_threshold =
    settings
      .value("xic/zivy_minmax_threshold", QString("%1").arg(_minmax_threshold))
      .toDouble();
  _maxmin_half_window = settings
                          .value("xic/zivy_maxmin_half_window",
                                 QString("%1").arg(_maxmin_half_window))
                          .toInt();
  _minmax_half_window = settings
                          .value("xic/zivy_minmax_half_window",
                                 QString("%1").arg(_minmax_half_window))
                          .toInt();
  _smoothing_half_window =
    settings
      .value("xic/zivy_smoothing", QString("%1").arg(_smoothing_half_window))
      .toInt();
}

ZivyWidget::ZivyWidget(QWidget *parent)
  : QWidget(parent), ui(new Ui::ZivyWidget)
{
  ui->setupUi(this);
#if QT_VERSION >= 0x050000
  // Qt5 code

#else
// Qt4 code
#endif
}

ZivyWidget::~ZivyWidget()
{
  qDebug() << "ZivyWidget::~ZivyWidget";
}

void
ZivyWidget::setZivyParams(const ZivyParams &params)
{
  ui->maxmin_spinbox->setValue(params._maxmin_half_window);
  ui->minmax_spinbox->setValue(params._minmax_half_window);
  ui->maxmin_threshold_spinbox->setValue(params._maxmin_threshold);
  ui->minmax_threshold_spinbox->setValue(params._minmax_threshold);
  ui->smoothing_spinbox->setValue(params._smoothing_half_window);
}

const ZivyParams
ZivyWidget::getZivyParams() const
{
  ZivyParams params;

  params._maxmin_half_window    = ui->maxmin_spinbox->value();
  params._minmax_half_window    = ui->minmax_spinbox->value();
  params._maxmin_threshold      = ui->maxmin_threshold_spinbox->value();
  params._minmax_threshold      = ui->minmax_threshold_spinbox->value();
  params._smoothing_half_window = ui->smoothing_spinbox->value();
  return params;
}

void
ZivyWidget::doSpinboxChanged(int value)
{
  emit zivyChanged(getZivyParams());
}
