/**
 * \file src/gui/xic_view/xic_widgets/zivydialog.h
 * \date 30/5/2018
 * \author Olivier Langella
 * \brief dialog window to edit Zivy quantification method
 */
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "zivydialog.h"
#include <QVBoxLayout>
#include <QDebug>

ZivyDialog::ZivyDialog(QWidget *parent)
{
  _p_zivy_widget = new ZivyWidget(this);
  _p_button_box =
    new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

  connect(_p_button_box, &QDialogButtonBox::accepted, this, &QDialog::accept);
  connect(_p_button_box, &QDialogButtonBox::rejected, this, &QDialog::reject);

  QVBoxLayout *mainLayout = new QVBoxLayout;
  mainLayout->addWidget(_p_zivy_widget);
  mainLayout->addWidget(_p_button_box);
  setLayout(mainLayout);

  setWindowTitle(tr("Edit parameters"));
}


ZivyDialog::~ZivyDialog()
{
  qDebug() << "ZivyDialog::~ZivyDialog";
}

void
ZivyDialog::setZivyParams(const ZivyParams &params)
{
  _p_zivy_widget->setZivyParams(params);
}

const ZivyParams
ZivyDialog::getZivyParams() const
{
  return _p_zivy_widget->getZivyParams();
}
