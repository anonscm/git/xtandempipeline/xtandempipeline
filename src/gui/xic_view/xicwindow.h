/**
 * \file src/gui/xic_view/xicwindow.h
 * \date 11/1/2018
 * \author Olivier Langella
 * \brief XIC window
 */
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QMainWindow>
#include <QAbstractButton>
#include <pappsomspp/xic/xic.h>
#include <pappsomspp/processing/detection/tracedetectionzivy.h>

#include "../../core/peptideevidence.h"
#include "xic_widgets/zivydialog.h"

class ProjectWindow;


namespace Ui
{
class XicWindow;
}

class XicBox;

class XicWindow : public QMainWindow
{
  Q_OBJECT
  friend XicBox;

  public:
  explicit XicWindow(ProjectWindow *parent = 0);
  ~XicWindow();

  void addXic(const PeptideEvidence *p_peptide_evidence);
  void addXicInMsRun(const PeptideEvidence *p_peptide_evidence,
                     MsRunSp msrun_sp);

  pappso::PrecisionPtr getXicExtractPrecision() const;
  pappso::XicExtractMethod getXicExtractionMethod() const;

  bool isRetentionTimeSeconds() const;

  void clear();
  public slots:
  void xicPrecisionChanged(pappso::PrecisionPtr precision);
  void rtUnitChanged(QAbstractButton *button);

  protected slots:
  void doEditZivyParams();
  void doAcceptedZivyDialog();
  void doXicExtractionMethodChanged(pappso::XicExtractMethod xic_method);

  signals:
  void reExtractXicNeeded();
  void rtUnitChangeNeeded();


  protected:
  ProjectWindow *getProjectWindow();
  void removeXicBox(XicBox *xic_box);

  void xicDetect(const pappso::Xic &xic,
                 pappso::TraceDetectionSinkInterface *sink) const;

  private:
  ProjectWindow *_project_window;
  Ui::XicWindow *ui;

  std::shared_ptr<pappso::TraceDetectionInterface> msp_detect_zivy;


  ZivyDialog *_p_zivy_dialog;
};
