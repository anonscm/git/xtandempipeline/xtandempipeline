
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "identificationgroupwidget.h"
#include "ui_identification_group_widget.h"
#include <QDebug>

IdentificationGroupWidget::IdentificationGroupWidget(QWidget *parent)
  : QWidget(parent), ui(new Ui::IdentificationGroupWidget)
{

  ui->setupUi(this);
  qDebug() << "IdentificationGroupWidget::IdentificationGroupWidget begin";

  qDebug() << "IdentificationGroupWidget::IdentificationGroupWidget end";
}

IdentificationGroupWidget::~IdentificationGroupWidget()
{
  qDebug() << "IdentificationGroupWidget::~IdentificationGroupWidget";
  delete ui;
  qDebug() << "IdentificationGroupWidget::~IdentificationGroupWidget end";
}

void
IdentificationGroupWidget::setIdentificationGroup(
  ProjectWindow *parent, IdentificationGroup *p_identification_group)
{
  _p_identification_group = p_identification_group;

  _p_project_window = parent;
#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(_p_project_window,
          &ProjectWindow::identificationGroupGrouped,
          this,
          &IdentificationGroupWidget::doIdentificationGroupGrouped);
#else
  // Qt4 code

  connect(_p_project_window,
          SIGNAL(identificationGroupGrouped(IdentificationGroup *)),
          this,
          SLOT(doIdentificationGroupGrouped(IdentificationGroup *)));
#endif
}

void
IdentificationGroupWidget::doViewPtmIslandList()
{
  qDebug() << "IdentificationGroupWidget::doViewPtmIslandList begin "
           << _p_identification_group;
  _p_project_window->doViewPtmIslandList(_p_identification_group);
  qDebug() << "IdentificationGroupWidget::doViewPtmIslandList end";
}


void
IdentificationGroupWidget::doViewProteinList()
{
  qDebug() << "IdentificationGroupWidget::doViewProteinList begin "
           << _p_identification_group;
  _p_project_window->doViewProteinList(_p_identification_group);
  qDebug() << "IdentificationGroupWidget::doViewProteinList end";
}

void
IdentificationGroupWidget::doViewMsIdentificationList()
{
  qDebug();
  _p_project_window->doViewMsIdentificationList();
  qDebug();
}

void
IdentificationGroupWidget::doIdentificationGroupGrouped(
  IdentificationGroup *p_identification_group)
{
  qDebug() << "IdentificationGroupWidget::doIdentificationGroupGrouped begin "
           << _p_identification_group << " " << p_identification_group;
  if(_p_identification_group == p_identification_group)
    {
      std::vector<MsRunSp> ms_run_list =
        _p_identification_group->getMsRunSpList();
      ui->sample_number_display->setText(QString("%1").arg(ms_run_list.size()));

      ui->group_number_display->setText(
        QString("%1").arg(_p_identification_group->countGroup()));
      ui->subgroup_number_display->setText(
        QString("%1").arg(_p_identification_group->countSubGroup()));
      ui->protein_number_display->setText(QString("%1").arg(
        _p_identification_group->countProteinMatch(ValidationState::grouped)));
      ui->grouped_peptide_display->setText(QString("%1").arg(
        _p_identification_group->countPeptideMass(ValidationState::grouped)));
      _p_identification_group->countPeptideMatch(ValidationState::grouped);
    }
  qDebug() << "IdentificationGroupWidget::doIdentificationGroupGrouped end "
           << _p_identification_group;
}
