/**
 * \file /gui/workerthread.cpp
 * \date 8/5/2017
 * \author Olivier Langella
 * \brief worker thread
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "workerthread.h"
#include <odsstream/odsdocwriter.h>
#include <odsstream/tsvoutputstream.h>
#include <odsstream/odsexception.h>
#include <pappsomspp/pappsoexception.h>
#include <QDebug>
#include "../output/exportfastafile.h"
#include "../output/masschroqml.h"
#include "../output/masschroqprm.h"
#include "../output/proticdbml.h"
#include "../output/ods/odsexport.h"
#include "../output/xpip.h"
#include "../output/mcqrspectralcount.h"
#include "files/xpipfile.h"
#include "mainwindow.h"
#include "project_view/projectwindow.h"
#include "../core/tandem_run/tandembatchprocess.h"
#include "../core/tandem_run/tandemcondorprocess.h"
#include <QSettings>

WorkerThread::WorkerThread(MainWindow *p_main_window)
{
  _p_main_window = p_main_window;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

  _p_work_monitor = new WorkMonitor();

#if QT_VERSION >= 0x050000
  // Qt5 code
  // worker message
  connect(_p_work_monitor,
          &WorkMonitor::workerMessage,
          p_main_window,
          &MainWindow::doDisplayLoadingMessage);
  connect(_p_work_monitor,
          &WorkMonitor::workerMessagePercent,
          p_main_window,
          &MainWindow::doDisplayLoadingMessagePercent);
  connect(_p_work_monitor,
          &WorkMonitor::workerAppendText,
          p_main_window,
          &MainWindow::doWorkerAppendText);
  connect(_p_work_monitor,
          &WorkMonitor::workerSetText,
          p_main_window,
          &MainWindow::doWorkerSetText);
  connect(_p_work_monitor,
          &WorkMonitor::workerJobFinished,
          p_main_window,
          &MainWindow::doDisplayJobFinished);
  connect(_p_work_monitor,
          &WorkMonitor::workerJobCanceled,
          p_main_window,
          &MainWindow::doDisplayJobCanceled);
  connect(p_main_window,
          &MainWindow::operateXpipFile,
          this,
          &WorkerThread::doXpipFileLoad);
  connect(p_main_window,
          &MainWindow::operateWritingXpipFile,
          this,
          &WorkerThread::doWritingXpipFile);
  connect(this,
          &WorkerThread::projectReady,
          p_main_window,
          &MainWindow::doProjectReady);
  connect(this,
          &WorkerThread::loadingResultsFinished,
          p_main_window,
          &MainWindow::doLoadingResultsReady);

  connect(p_main_window,
          &MainWindow::operateLoadingResults,
          this,
          &WorkerThread::doLoadingResults);

  connect(p_main_window,
          &MainWindow::operateRunningXtandem,
          this,
          &WorkerThread::doRunningXtandem);
  connect(this,
          &WorkerThread::loadingMessage,
          p_main_window,
          &MainWindow::doDisplayLoadingMessage);
  connect(this,
          &WorkerThread::projectNotReady,
          p_main_window,
          &MainWindow::doProjectNotReady);

  // grouping
  connect(p_main_window,
          &MainWindow::operateGrouping,
          this,
          &WorkerThread::doGrouping);
  connect(this,
          &WorkerThread::groupingFinished,
          p_main_window,
          &MainWindow::doGroupingFinished);
  // masschroq write
  connect(p_main_window,
          &MainWindow::operateWritingMassChroqFile,
          this,
          &WorkerThread::doWritingMassChroqFile);
  connect(p_main_window,
          &MainWindow::operateWritingMassChroqPrmFile,
          this,
          &WorkerThread::doWritingMassChroqPrmFile);
  connect(p_main_window,
          &MainWindow::operateWritingMcqrSpectralCountFile,
          this,
          &WorkerThread::doWritingMcqrSpectralCountFile);
  // protic write
  connect(p_main_window,
          &MainWindow::operateWritingProticFile,
          this,
          &WorkerThread::doWritingProticFile);
  // writing ODS file :
  connect(p_main_window,
          &MainWindow::operateWritingOdsFile,
          this,
          &WorkerThread::doWritingOdsFile);
  connect(this,
          &WorkerThread::operationFailed,
          p_main_window,
          &MainWindow::doOperationFailed);
  connect(this,
          &WorkerThread::operationFinished,
          p_main_window,
          &MainWindow::doOperationFinished);
  // FASTA file write
  connect(p_main_window,
          &MainWindow::operateWritingFastaFile,
          this,
          &WorkerThread::doWritingFastaFile);

#else
  // Qt4 code
  // worker message
  connect(_p_work_monitor,
          SIGNAL(workerMessage(QString)),
          p_main_window,
          SLOT(doDisplayLoadingMessage(QString)));
  connect(_p_work_monitor,
          SIGNAL(workerMessage(QString, int)),
          p_main_window,
          SLOT(doDisplayLoadingMessage(QString, int)));
  connect(_p_work_monitor,
          SIGNAL(workerAppendText(char *)),
          p_main_window,
          SLOT(doWorkerAppendText(char *)));

  connect(p_main_window,
          SIGNAL(operateXpipFile(QString)),
          this,
          SLOT(doXpipFileLoad(QString)));
  connect(this,
          SIGNAL(projectReady(ProjectSp)),
          p_main_window,
          SLOT(doProjectReady(ProjectSp)));
  connect(
    p_main_window,
    SIGNAL(operateLoadingResults(bool, AutomaticFilterParameters, QStringList)),
    this,
    SLOT(doLoadingResults(bool, AutomaticFilterParameters, QStringList)));

  connect(p_main_window,
          SIGNAL(operateRunningXtandem(TandemRunBatch)),
          this,
          SLOT(doRunningXtandem(TandemRunBatch)));
  connect(this,
          SIGNAL(loadingMessage(QString)),
          p_main_window,
          SLOT(doDisplayLoadingMessage(QString)));
  connect(this,
          SIGNAL(projectNotReady(QString)),
          p_main_window,
          SLOT(doProjectNotReady(QString)));

  // grouping
  connect(p_main_window,
          SIGNAL(operateGrouping(ProjectSp)),
          this,
          SLOT(doGrouping(ProjectSp)));
  connect(this,
          SIGNAL(groupingFinished()),
          p_main_window,
          SLOT(doGroupingFinished()));
  // masschroq write
  connect(p_main_window,
          SIGNAL(operateWritingMassChroqFile(QString, ProjectSp)),
          this,
          SLOT(doWritingMassChroqFile(QString, ProjectSp)));
  // protic write
  connect(p_main_window,
          SIGNAL(operateWritingProticFile(QString, ProjectSp)),
          this,
          SLOT(doWritingProticFile(QString, ProjectSp)));
  // writing ODS file :
  connect(p_main_window,
          SIGNAL(operateWritingOdsFile(QString, ProjectSp)),
          this,
          SLOT(doWritingOdsFile(QString, ProjectSp)));
  connect(this,
          SIGNAL(operationFailed(QString)),
          p_main_window,
          SLOT(doOperationFailed(QString)));
  connect(this,
          SIGNAL(operationFinished()),
          p_main_window,
          SLOT(doOperationFinished()));
#endif

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

WorkerThread::WorkerThread(ProjectWindow *p_project_window)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

  _p_work_monitor = new WorkMonitor();

#if QT_VERSION >= 0x050000
  // Qt5 code

  // worker message
  connect(_p_work_monitor,
          &WorkMonitor::workerMessage,
          p_project_window,
          &ProjectWindow::doDisplayLoadingMessage);
  connect(_p_work_monitor,
          &WorkMonitor::workerMessagePercent,
          p_project_window,
          &ProjectWindow::doDisplayLoadingMessagePercent);
  connect(this,
          &WorkerThread::loadingMessage,
          p_project_window,
          &ProjectWindow::doDisplayLoadingMessage);
  // grouping
  connect(p_project_window,
          &ProjectWindow::operateGrouping,
          this,
          &WorkerThread::doGrouping);
  connect(this,
          &WorkerThread::groupingFinished,
          p_project_window,
          &ProjectWindow::doGroupingFinished);


  // PTM grouping on IdentificationGroup
  connect(p_project_window,
          &ProjectWindow::operatePtmGroupingOnIdentification,
          this,
          &WorkerThread::doPtmGroupingOnIdentification);
  connect(this,
          &WorkerThread::ptmGroupingOnIdentificationFinished,
          p_project_window,
          &ProjectWindow::refreshPtmGroup);

  // grouping on IdentificationGroup
  connect(p_project_window,
          &ProjectWindow::operateGroupingOnIdentification,
          this,
          &WorkerThread::doGroupingOnIdentification);
  connect(this,
          &WorkerThread::groupingOnIdentificationFinished,
          p_project_window,
          &ProjectWindow::refreshGroup);

  connect(this,
          &WorkerThread::operationFailed,
          p_project_window,
          &ProjectWindow::doOperationFailed);
  connect(this,
          &WorkerThread::operationFinished,
          p_project_window,
          &ProjectWindow::doOperationFinished);

#else
  // Qt4 code
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

  // worker message
  connect(_p_work_monitor,
          SIGNAL(workerMessage(QString)),
          p_project_window,
          SLOT(doDisplayLoadingMessage(QString)));
  connect(_p_work_monitor,
          SIGNAL(workerMessage(QString, int)),
          p_project_window,
          SLOT(doDisplayLoadingMessage(QString, int)));

  connect(this,
          SIGNAL(loadingMessage(QString)),
          p_project_window,
          SLOT(doDisplayLoadingMessage(QString)));
  // grouping
  connect(p_project_window,
          SIGNAL(operateGrouping(ProjectSp)),
          this,
          SLOT(doGrouping(ProjectSp)));
  connect(this,
          SIGNAL(groupingFinished()),
          p_project_window,
          SLOT(doGroupingFinished()));


  // PTM grouping on IdentificationGroup
  connect(p_project_window,
          SIGNAL(operatePtmGroupingOnIdentification(IdentificationGroup *)),
          this,
          SLOT(doPtmGroupingOnIdentification(IdentificationGroup *)));
  connect(this,
          SIGNAL(ptmGroupingOnIdentificationFinished(IdentificationGroup *)),
          p_project_window,
          SLOT(refreshPtmGroup(IdentificationGroup *)));

  // grouping on IdentificationGroup
  connect(
    p_project_window,
    SIGNAL(
      operateGroupingOnIdentification(IdentificationGroup *, GroupingType)),
    this,
    SLOT(doGroupingOnIdentification(IdentificationGroup *, GroupingType)));
  connect(this,
          SIGNAL(groupingOnIdentificationFinished(IdentificationGroup *)),
          p_project_window,
          SLOT(refreshGroup(IdentificationGroup *)));

  connect(this,
          SIGNAL(operationFailed(QString)),
          p_project_window,
          SLOT(doOperationFailed(QString)));
  connect(this,
          SIGNAL(operationFinished()),
          p_project_window,
          SLOT(doOperationFinished()));

#endif
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

WorkerThread::~WorkerThread()
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

void
WorkerThread::doXpipFileLoad(QString filename)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << filename;
  try
    {
      QFileInfo new_xpip_file(filename);
      emit loadingMessage(tr("loading XPIP file"));
      qDebug() << "WorkerThread::doXpipFileLoad new_xpip_file "
               << new_xpip_file.absoluteFilePath();
      XpipFile xpip_file(new_xpip_file);
      ProjectSp project_sp = xpip_file.getProjectSp(_p_work_monitor);
      project_sp.get()->setProjectName(filename);
      emit projectReady(project_sp);
    }
  catch(pappso::PappsoException &error)
    {
      emit projectNotReady(
        tr("Error while reading XPIP file :\n%1").arg(error.qwhat()));
    }

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

void
WorkerThread::doPtmGroupingOnIdentification(
  IdentificationGroup *p_identification_group)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  try
    {
      emit loadingMessage(tr("computing PTM islands"));
      try
        {
          p_identification_group->startPtmGrouping();
        }
      catch(pappso::PappsoException &exception_pappso)
        {
          emit operationFailed(tr("Error computing PTM islands : %1")
                                 .arg(exception_pappso.qwhat()));
        }
      catch(std::exception &exception_std)
        {
          emit operationFailed(
            tr("Error computing PTM islands : %1").arg(exception_std.what()));
        }
      emit loadingMessage(tr("computing PTM islands finished"));
      emit ptmGroupingOnIdentificationFinished(p_identification_group);
    }
  catch(pappso::PappsoException &error)
    {
      emit operationFailed(
        tr("Error while computing PTM islands :\n%1").arg(error.qwhat()));
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

void
WorkerThread::doGroupingOnIdentification(
  IdentificationGroup *p_identification_group,
  ContaminantRemovalMode contaminant_removal_mode,
  GroupingType grouping_type)
{
  qDebug() << "TEST";
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  try
    {
      emit loadingMessage(tr("grouping proteins"));
      try
        {
          p_identification_group->startGrouping(
            contaminant_removal_mode, grouping_type, _p_work_monitor);
        }
      catch(pappso::PappsoException &exception_pappso)
        {
          emit operationFailed(tr("Error grouping identification : %1")
                                 .arg(exception_pappso.qwhat()));
        }
      catch(std::exception &exception_std)
        {
          emit operationFailed(
            tr("Error grouping identification : %1").arg(exception_std.what()));
        }
      emit loadingMessage(tr("grouping proteins finished"));
      emit groupingOnIdentificationFinished(p_identification_group);
    }
  catch(pappso::PappsoException &error)
    {
      emit operationFailed(tr("Error while grouping :\n%1").arg(error.qwhat()));
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}
void
WorkerThread::doGrouping(ProjectSp project_sp)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  try
    {
      emit loadingMessage(tr("grouping proteins"));
      try
        {
          project_sp.get()->startGrouping(_p_work_monitor);
        }
      catch(pappso::PappsoException &exception_pappso)
        {
          emit operationFailed(
            tr("Error grouping project : %1").arg(exception_pappso.qwhat()));
        }
      catch(std::exception &exception_std)
        {
          emit operationFailed(
            tr("Error grouping project : %1").arg(exception_std.what()));
        }
      emit loadingMessage(tr("grouping proteins finished"));
      emit groupingFinished();
    }
  catch(pappso::PappsoException &error)
    {
      emit operationFailed(tr("Error while grouping :\n%1").arg(error.qwhat()));
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

void
WorkerThread::doWritingXpipFile(QString filename, ProjectSp project_sp)
{

  try
    {
      emit loadingMessage(tr("writing XPIP file, please wait"));
      project_sp.get()->checkPsimodCompliance();
      Xpip xpip_file(filename);
      xpip_file.write(project_sp);
      xpip_file.close();

      emit operationFinished();
    }
  catch(pappso::PappsoException &error)
    {
      emit operationFailed(
        tr("Error while writing XPIP file :\n%1").arg(error.qwhat()));
    }
}
void
WorkerThread::doWritingOdsFile(QString filename,
                               QString format,
                               ProjectSp project_sp)
{

  try
    {

      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
      CalcWriterInterface *p_writer = nullptr;
      if(format == "tsv")
        {
          emit loadingMessage(tr("writing TSV files, please wait"));
          p_writer = new TsvDirectoryWriter(filename);
        }
      else
        {
          emit loadingMessage(tr("writing ODS file, please wait"));
          p_writer = new OdsDocWriter(filename);
        }

      OdsExport export_ods(project_sp);
      try
        {
          export_ods.write(p_writer, _p_work_monitor);
        }
      catch(pappso::PappsoException &error)
        {
          p_writer->close();
          delete p_writer;
          throw error;
        }
      p_writer->close();
      delete p_writer;
      emit operationFinished();
    }
  catch(pappso::PappsoException &error)
    {

      emit operationFailed(
        tr("Error while writing ODS file :\n%1").arg(error.qwhat()));
    }
}


void
WorkerThread::doWritingMassChroqFile(QString filename,
                                     ProjectSp project_sp,
                                     MasschroqFileParameters params)
{

  try
    {
      emit loadingMessage(tr("writing MassChroqML file, please wait"));

      project_sp.get()->checkPsimodCompliance();
      MassChroQml output(filename, params);
      output.write(project_sp);
      output.close();
      emit operationFinished();
    }
  catch(pappso::PappsoException &error)
    {
      emit operationFailed(
        tr("Error while writing MassChroqML file :\n%1").arg(error.qwhat()));
    }
}

void
WorkerThread::doWritingMassChroqPrmFile(QString filename, ProjectSp project_sp)
{

  try
    {
      emit loadingMessage(tr("writing MassChroqPRM file, please wait"));

      project_sp.get()->checkPsimodCompliance();
      MassChroqPrm output(filename);
      output.write(project_sp);
      output.close();
      emit operationFinished();
    }
  catch(pappso::PappsoException &error)
    {
      emit operationFailed(
        tr("Error while writing MassChroqPRM file :\n%1").arg(error.qwhat()));
    }
}

void
WorkerThread::doWritingProticFile(QString filename, ProjectSp project_sp)
{

  try
    {
      emit loadingMessage(tr("writing PROTICdbML file, please wait"));

      project_sp.get()->checkPsimodCompliance();
      ProticdbMl output(filename);
      output.write(project_sp);
      output.close();
      emit operationFinished();
    }
  catch(pappso::PappsoException &error)
    {
      emit operationFailed(
        tr("Error while writing PROTICdbML file :\n%1").arg(error.qwhat()));
    }
}


void
WorkerThread::doWritingMcqrSpectralCountFile(QString filename,
                                             ProjectSp project_sp)
{
  try
    {
      emit loadingMessage(
        tr("Writing %1 spectral count file for MassChroqR").arg(filename));
      QFile outFile;
      outFile.setFileName(filename);
      outFile.open(QIODevice::WriteOnly);
      QTextStream *p_outputStream = new QTextStream(&outFile);
      TsvOutputStream *p_writer   = new TsvOutputStream(*p_outputStream);
      p_writer->setNoSheetName(true);
      McqrSpectralCount spectra_sheet(p_writer, project_sp.get());
      spectra_sheet.writeSheet();

      p_writer->close();
      delete p_writer;
      delete p_outputStream;
      outFile.close();
      emit operationFinished();
    }
  catch(OdsException &error)
    {
      emit operationFailed(
        tr("Error while writing spectral count file for MassChroqR :\n%1")
          .arg(error.qwhat()));
    }
  catch(pappso::PappsoException &errorb)
    {
      emit operationFailed(
        tr("Error while writing spectral count file for MassChroqR :\n%1")
          .arg(errorb.qwhat()));
    }
}


void
WorkerThread::doLoadingResults(bool is_individual,
                               AutomaticFilterParameters param,
                               QStringList file_list)
{

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  try
    {
      if(file_list.size() == 0)
        {
          throw pappso::PappsoException(QObject::tr("file list is empty"));
        }
      ProjectSp project_sp = Project().makeProjectSp();
      if(is_individual)
        {
          project_sp.get()->setProjectMode(ProjectMode::individual);
        }
      else
        {
          project_sp.get()->setProjectMode(ProjectMode::combined);
        }

      _p_work_monitor->setProgressMaximumValue(file_list.size());
      int i = 0;
      for(QString filename : file_list)
        {
          _p_work_monitor->message(tr("loading result file %1 (%2 on %3)")
                                     .arg(filename)
                                     .arg(i)
                                     .arg(file_list.size()),
                                   i);
          project_sp.get()->readResultFile(filename);
          i++;
        }

      emit loadingMessage(tr("filtering proteins"));
      project_sp.get()->updateAutomaticFilters(param);
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
      emit loadingResultsFinished(project_sp);
    }
  catch(pappso::PappsoException &error)
    {
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
               << error.qwhat();
      emit projectNotReady(
        tr("Error while reading result files :\n%1").arg(error.qwhat()));
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}
void
WorkerThread::doRunningXtandem(TandemRunBatch tandem_run_batch)
{
  qDebug();
  try
    {
      emit loadingMessage(tr("Running X!Tandem, please wait"));
      qDebug() << "WorkerThread::doRunningXtandem tandem_run_batch "
               << tandem_run_batch._tandem_bin_path;

      QSettings settings;
      bool use_htcondor =
        settings.value("tandem/use_HTCondor", "false").toBool();

      if(use_htcondor)
        {
          TandemCondorProcess xt_process(
            _p_main_window, _p_work_monitor, tandem_run_batch);
          xt_process.run();
        }
      else
        {
          TandemBatchProcess xt_process(
            _p_main_window, _p_work_monitor, tandem_run_batch);
          xt_process.run();
        }

      emit operationFinished();
    }
  catch(pappso::PappsoException &error)
    {
      qDebug() << error.qwhat();
      emit operationFailed(
        tr("Error while running X!Tandem job :\n%1").arg(error.qwhat()));
    }
  qDebug();
}


void
WorkerThread::doWritingFastaFile(QString filename,
                                 ProjectSp project_sp,
                                 ExportFastaType type)
{

  try
    {

      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
      emit loadingMessage(tr("writing FASTA file, please wait"));


      ExportFastaFile output(filename, type);
      output.write(project_sp);
      output.close();

      // emit operateXpipFile(filename);
      emit operationFinished();
    }
  catch(pappso::PappsoException &error)
    {
      emit operationFailed(
        tr("Error while writing FASTA file :\n%1").arg(error.qwhat()));
    }
}


void
WorkerThread::doFindBestMsrunForAlignment(ProjectSp project_sp)
{
  try
    {
      qDebug();
      emit loadingMessage(
        tr("looking for the best MSrun reference for alignment, please wait"));

      _p_work_monitor->message("checking MS run file path");
      _p_work_monitor->setProgressMaximumValue(
        project_sp->getMsRunStore().getMsRunList().size());
      bool is_ok = true;

      checkMsrunFilePath(project_sp->getMsRunStore().getMsRunList());

      _p_work_monitor->message("preparing MS run retention times");
      try
        {
          if(!_p_main_window->stopWorkerThread())
            {
              project_sp->prepareMsrunRetentionTimesForAlignment();
            }
        }
      catch(pappso::PappsoException &error)
        {
          throw pappso::PappsoException(
            tr("Error while preparing MS run retention times :\n%1")
              .arg(error.qwhat()));
        }

      try
        {
          MsRunSp ms_run_best_reference;

          if(!_p_main_window->stopWorkerThread())
            {
              ms_run_best_reference =
                chooseBestMsRun(project_sp->getMsRunStore().getMsRunList());

              if(ms_run_best_reference != nullptr)
                {
                  qDebug() << " " << ms_run_best_reference.get()->getXmlId();
                }
            }
          _p_work_monitor->message("done");
          // emit operateXpipFile(filename);

          if(_p_main_window->stopWorkerThread())
            {
              _p_work_monitor->canceled("Finding the best MsRun canceled!");
            }
          else
            {
              emit findingBestMsrunForAlignmentFinished(ms_run_best_reference);
            }
        }
      catch(pappso::PappsoException &error)
        {
          throw pappso::PappsoException(
            tr("Error while choosing best MS run :\n%1").arg(error.qwhat()));
        }
    }
  catch(pappso::PappsoException &error)
    {
      emit operationFailed(
        tr("Error looking for the best MSrun reference for alignment :\n%1")
          .arg(error.qwhat()));
    }
}

void
WorkerThread::doFindBestMsrunInAlignmentGroup(MsRunAlignmentGroupSp group)
{
  try
    {
      qDebug();
      emit loadingMessage(
        tr("looking for the best MSrun reference for alignment in the group, "
           "please wait"));

      std::list<MsRunSp> msruns_list = group->getMsRunsInAlignmentGroup();
      std::vector<MsRunSp> msruns(msruns_list.size());
      std::copy(msruns_list.begin(), msruns_list.end(), msruns.begin());

      _p_work_monitor->message("checking MS run file path");
      _p_work_monitor->setProgressMaximumValue(
        group->getMsRunsInAlignmentGroup().size());

      checkMsrunFilePath(msruns);

      _p_work_monitor->message("preparing MS run retention times");
      try
        {
          if(!_p_main_window->stopWorkerThread())
            {
              group->prepareMsrunRetentionTimesForAlignment();
            }
        }
      catch(pappso::PappsoException &error)
        {
          throw pappso::PappsoException(
            tr("Error while preparing MS run retention times :\n%1")
              .arg(error.qwhat()));
        }

      try
        {
          MsRunSp ms_run_best_reference;

          if(!_p_main_window->stopWorkerThread())
            {
              ms_run_best_reference = chooseBestMsRun(msruns);

              if(ms_run_best_reference != nullptr)
                {
                  qDebug() << " " << ms_run_best_reference.get()->getXmlId();
                }
            }
          _p_work_monitor->message("done");
          // emit operateXpipFile(filename);

          if(_p_main_window->stopWorkerThread())
            {
              _p_work_monitor->canceled("Finding the best MsRun canceled!");
            }
          else
            {
              emit bestMsrunForAlignmentGroupFinished(ms_run_best_reference);
            }
        }
      catch(pappso::PappsoException &error)
        {
          throw pappso::PappsoException(
            tr("Error while choosing best MS run :\n%1").arg(error.qwhat()));
        }
    }
  catch(pappso::PappsoException &error)
    {
      emit operationFailed(
        tr("Error looking for the best MSrun reference for alignment :\n%1")
          .arg(error.qwhat()));
    }
}


void
WorkerThread::checkMsrunFilePath(std::vector<MsRunSp> msruns)
{
  std::size_t i      = 0;
  bool should_i_stop = false;
  for(auto &msrun_sp : msruns)
    {
      should_i_stop = _p_main_window->stopWorkerThread();
      if(!should_i_stop)
        {
          _p_work_monitor->message(tr("checking MS run file path for sample %1")
                                     .arg(msrun_sp->getSampleName()),
                                   i);
          if(msrun_sp->findMsRunFile())
            {
            }
          else
            {
              throw pappso::PappsoException(
                tr("Error while checking MS run file path :\nMSrun \"%1\" "
                   "for "
                   "sample named \"%2\" not found")
                  .arg(msrun_sp->getXmlId())
                  .arg(msrun_sp->getSampleName()));
            }
          i++;
        }
    }
}

MsRunSp
WorkerThread::chooseBestMsRun(std::vector<MsRunSp> msruns)
{
  _p_work_monitor->message("choosing MS run");

  MsRunSp ms_run_best_reference;
  std::size_t best_number = 0;
  bool should_i_stop      = false;

  for(auto &msrun_ref_sp : msruns)
    {
      should_i_stop = _p_main_window->stopWorkerThread();
      if(!should_i_stop)
        {
          if(ms_run_best_reference == nullptr)
            {
              ms_run_best_reference = msrun_ref_sp;
            }

          std::size_t min_number = std::numeric_limits<std::size_t>::max();
          for(auto &msrun_sp : msruns)
            {
              should_i_stop = _p_main_window->stopWorkerThread();
              if(!should_i_stop)
                {
                  if(msrun_sp.get() != msrun_ref_sp.get())
                    {
                      pappso::Trace trace;
                      trace =
                        msrun_sp.get()
                          ->getMsRunRetentionTimePtr()
                          ->getCommonDeltaRt(msrun_ref_sp.get()
                                               ->getMsRunRetentionTimePtr()
                                               ->getSeamarks());
                      if(trace.size() < min_number)
                        {
                          min_number = trace.size();
                        }
                    }
                }
            }

          if(min_number > best_number)
            {
              best_number = min_number;

              ms_run_best_reference = msrun_ref_sp;
            }
        }
    }
  return ms_run_best_reference;
}


void
WorkerThread::doCheckMsrunFilePath(ProjectSp msp_project)
{

  MsRunSp msrun_sp = nullptr;
  try
    {

      _p_work_monitor->setProgressMaximumValue(
        msp_project->getIdentificationDataSourceStore()
          .getIdentificationDataSourceList()
          .size());

      std::size_t i = 0;
      for(IdentificationDataSourceSp p_ident_data_source :
          msp_project->getIdentificationDataSourceStore()
            .getIdentificationDataSourceList())
        {
          msrun_sp = p_ident_data_source->getMsRunSp();
          _p_work_monitor->message(
            tr("checking MS run file path for \"%1\", named \"%2\"")
              .arg(msrun_sp.get()->getXmlId())
              .arg(msrun_sp.get()->getSampleName()),
            i);

          if(msrun_sp->findMsRunFile())
            {
              // msrun_sp->checkMsRunStatistics();
              msrun_sp = nullptr;
            }
          else
            {
              // is_ok = false;
              break;
            }
          i++;
        }
    }

  catch(pappso::PappsoException &error)
    {
      emit operationFailed(
        tr("Error checking MSrun files path :\n%1").arg(error.qwhat()));
    }

  emit checkingMsrunFilePathFinished(msrun_sp);
}
