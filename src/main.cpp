
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "config.h"
#include "gui/mainwindow.h"
#include "utils/types.h"
#include <QApplication>
#include <QTimer>
#include <iostream>
#include <pappsomspp/pappsoexception.h>

using namespace std;

int
main(int argc, char *argv[])
{
  QTextStream errorStream(stderr, QIODevice::WriteOnly);
  QApplication app(argc, argv);

  qRegisterMetaType<TandemRunBatch>("TandemRunBatch");
  qRegisterMetaType<std::vector<pappso::pappso_double>>(
    "std::vector<pappso::pappso_double>");
  qRegisterMetaType<pappso::PrecisionPtr>("pappso::PrecisionPtr");
  qRegisterMetaType<std::vector<pappso::XicCstSPtr>>(
    "std::vector<pappso::XicCstSPtr>");
  qRegisterMetaType<pappso::PeptideSp>("pappso::PeptideSp");
  qRegisterMetaType<pappso::XicExtractMethod>("pappso::XicExtractMethod");
  qRegisterMetaType<pappso::MassSpectrumCstSPtr>("pappso::MassSpectrumCstSPtr");
  qRegisterMetaType<MsRunSp>("MsRunSp");
  qRegisterMetaType<std::vector<MsRunSp>>("<std::vector<MsRunSp>>");
  qRegisterMetaType<MsRunAlignmentGroupSp>("MsRunAlignmentGroupSp");
  qRegisterMetaType<std::vector<pappso::PeptideNaturalIsotopeAverageSp>>(
    "std::vector<pappso::PeptideNaturalIsotopeAverageSp>");
  qRegisterMetaType<ExportFastaType>("ExportFastaType");
  qRegisterMetaType<MasschroqFileParameters>("MasschroqFileParameters");
  qRegisterMetaType<IdentificationDataSourceSp>("IdentificationDataSourceSp");
  qRegisterMetaType<ContaminantRemovalMode>("ContaminantRemovalMode");

  // qRegisterMetaType<pappso::PeakIonIsotopeMatch>("pappso::PeakIonIsotopeMatch");

  try
    {
      QCoreApplication::setOrganizationName("PAPPSO");
      QCoreApplication::setOrganizationDomain("pappso.inra.fr");
      QCoreApplication::setApplicationName("xtpcpp");
      MainWindow window;
      window.show();

      // This code will start the messaging engine in QT and in
      // 10ms it will start the execution in the MainClass.run routine;
      QTimer::singleShot(10, &window, SLOT(run()));

      return app.exec();
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in XTPcpp. Dont Panic :" << endl;
      errorStream << error.qwhat() << endl;
      app.exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in XTPcpp. Dont Panic :" << endl;
      errorStream << error.what() << endl;
      app.exit(1);
    }
}
