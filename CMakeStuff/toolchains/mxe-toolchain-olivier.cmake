# This file should be included if the command line reads like this:
# x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1 ..

MESSAGE("MXE (M cross environment) https://mxe.cc/")
message("Please run the configuration like this:")
message("x86_64-w64-mingw32.shared-cmake -DMXE=1 -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ../../development")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /media/langella/pappso/mxe/usr/x86_64-w64-mingw32.shared/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /media/langella/pappso/mxe/usr/x86_64-w64-mingw32.shared/include)


if(WIN32 OR _WIN32)
	message(STATUS "Building with WIN32 defined.")
endif()


set (QCustomPlot_FOUND TRUE)
set (QCustomPlot_INCLUDES "/home/langella/developpement/git/qcustomplot")
set (QCustomPlot_LIBRARIES "/home/langella/developpement/git/qcustomplot/wbuild/libqcustomplot.dll")


#sudo apt install libc6-dev-i386
   # QUAZIP_INCLUDE_DIR - Path to QuaZip include dir
set (QUAZIP_INCLUDE_DIR "/home/langella/developpement/git/quazip")
# QUAZIP_INCLUDE_DIRS- Path to QuaZip and zlib include dir (combined from QUAZIP_INCLUDE_DIR + ZLIB_INCLUDE_DIR)
# QUAZIP_LIBRARIES   - List of QuaZip libraries
set (QUAZIP_QT5_LIBRARIES "/home/langella/developpement/git/quazip/wbuild/libquazip5.dll")
set (QUAZIP_LIBRARIES "/home/langella/developpement/git/quazip/wbuild/libquazip5.dll")
# QUAZIP_ZLIB_INCLUDE_DIR- The include dir of zlib headers

set(ODSSTREAM_QT5_FOUND 1)
set(ODSSTREAM_INCLUDE_DIR "/home/langella/developpement/git/libodsstream/src")
set(ODSSTREAM_QT5_LIBRARY "/home/langella/developpement/git/libodsstream/wbuild/src/libodsstream-qt5.dll")
if(NOT TARGET OdsStream::Core)
    add_library(OdsStream::Core UNKNOWN IMPORTED)
        set_target_properties(OdsStream::Core PROPERTIES
        IMPORTED_LOCATION "${ODSSTREAM_QT5_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${ODSSTREAM_INCLUDE_DIR}"
    )
endif()


set(PAPPSOMSPP_QT5_FOUND 1)
set(PAPPSOMSPP_WIDGET_QT5_FOUND 1)
set(PAPPSOMSPP_INCLUDE_DIR "/home/langella/developpement/git/pappsomspp/src")
mark_as_advanced(PAPPSOMSPP_INCLUDE_DIR)
set(PAPPSOMSPP_QT5_LIBRARY "/home/langella/developpement/git/pappsomspp/wbuild/src/libpappsomspp-qt5.dll")
mark_as_advanced(PAPPSOMSPP_QT5_LIBRARY)
set(PAPPSOMSPP_WIDGET_QT5_LIBRARY "/home/langella/developpement/git/pappsomspp/wbuild/src/pappsomspp/widget/libpappsomspp-widget-qt5.dll")
mark_as_advanced(PAPPSOMSPP_WIDGET_QT5_LIBRARY)

if(NOT TARGET Pappso::Core)
    add_library(Pappso::Core UNKNOWN IMPORTED)
    set_target_properties(Pappso::Core PROPERTIES
    IMPORTED_LOCATION "${PAPPSOMSPP_QT5_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${PAPPSOMSPP_INCLUDE_DIR}")
endif()


if(NOT TARGET Pappso::Widget)
    add_library(Pappso::Widget UNKNOWN IMPORTED)
    set_target_properties(Pappso::Widget PROPERTIES
    IMPORTED_LOCATION "${PAPPSOMSPP_WIDGET_QT5_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${PAPPSOMSPP_INCLUDE_DIR}")
endif()

   

set(Pwiz_FOUND 1)
set(Pwiz_INCLUDE_DIR "/home/langella/developpement/git/custompwiz/src")
set(Pwiz_LIBRARY "/home/langella/developpement/git/custompwiz/wbuild/src/libcustompwiz.dll")

if(NOT TARGET Pwiz::Pwiz)
    add_library(Pwiz::Pwiz UNKNOWN IMPORTED)
    set_target_properties(Pwiz::Pwiz PROPERTIES
    IMPORTED_LOCATION "${Pwiz_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${Pwiz_INCLUDE_DIR}")
endif()
