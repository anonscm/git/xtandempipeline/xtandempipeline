message("UNIX non APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)

find_package( Odsstream REQUIRED )

if (PAPPSOMSPP_QT5_FOUND)
else (PAPPSOMSPP_QT5_FOUND)
    set(Pappsomspp_DIR ${CMAKE_MODULE_PATH})
    find_package( Pappsomspp REQUIRED )
endif (PAPPSOMSPP_QT5_FOUND)

#find_package( Boost COMPONENTS iostreams thread filesystem chrono REQUIRED )
find_package( Boost COMPONENTS thread filesystem iostreams REQUIRED ) 



set(Alglib_DIR ${CMAKE_MODULE_PATH})
find_package(Alglib REQUIRED)


find_package( ZLIB REQUIRED )

set(Zstd_DIR ${CMAKE_MODULE_PATH})
find_package(Zstd REQUIRED)

set(Pwiz_DIR ${CMAKE_MODULE_PATH})
find_package(Pwiz REQUIRED)
