#download Qt5 mingw64 environment from :
#http://www.rpmfind.net//linux/RPM/fedora/21/x86_64/m/mingw64-qt5-qtbase-5.3.2-1.fc21.noarch.html
#ftp://195.220.108.108/linux/sourceforge/p/ps/pspp4windows/used-repositories/2015-06-19/win64/src/mingw64-zlib-1.2.8-8.13.src.rpm
#http://rpm.pbone.net/index.php3/stat/4/idpl/24061932/dir/opensuse_12.x/com/mingw64-quazip-0.4.4-2.39.noarch.rpm.html

# cd buildwin64
# cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../win64/Toolchain-mingw64.cmake ..


SET (USEQT5 1)
set (MINGW 1)
set (MAKE_TEST 0)

set(CMAKE_CXX_COMPILER "/mingw64/bin/g++.exe")


set(ODSSTREAM_QT5_FOUND 1)
set(ODSSTREAM_INCLUDE_DIR "/home/polipo/devel/libodsstream/src")
set(ODSSTREAM_QT5_LIBRARY "/home/polipo/devel/libodsstream/build/src/libodsstream-qt5.so")


# QUAZIP_INCLUDE_DIR         - Path to QuaZip include dir
set (QUAZIP_INCLUDE_DIR "/home/polipo/devel/quazip-0.7.3")
# QUAZIP_INCLUDE_DIRS        - Path to QuaZip and zlib include dir (combined from QUAZIP_INCLUDE_DIR + ZLIB_INCLUDE_DIR)
# QUAZIP_LIBRARIES           - List of QuaZip libraries
set (QUAZIP_QT5_LIBRARIES "/home/polipo/devel/quazip-0.7.3/quazip/release/quazip.dll")
# QUAZIP_ZLIB_INCLUDE_DIR    - The include dir of zlib headers

set (QCustomPlot_FOUND 1)
set (QCustomPlot_INCLUDES "/home/polipo/devel/qcustomplot-1.3.2+dfsg1")
set (QCustomPlot_LIBRARIES "/home/polipo/devel/qcustomplot-1.3.2+dfsg1/build/libqcustomplot.so")

set(PAPPSOMSPP_QT5_FOUND 1)
set(PAPPSOMSPP_WIDGET_QT5_FOUND 1)
set(PAPPSOMSPP_INCLUDE_DIR "/home/polipo/devel/pappsomspp/src")
set(PAPPSOMSPP_QT5_LIBRARY "/home/polipo/devel/pappsomspp/build/src/libpappsomspp-qt5.so")
set(PAPPSOMSPP_WIDGET_QT5_LIBRARY "/home/polipo/devel/pappsomspp/build/src/pappsomspp/widget/libpappsomspp-widget-qt5.so")


set(ZLIB_LIBRARIES "/mingw64/lib/libz.a")


set(Pwiz_FOUND 1)
set(Pwiz_INCLUDE_DIR "/home/polipo/devel/pwiz")
set(Pwiz_LIBRARY "/home/polipo/devel/pwiz/libpwiz.a")

set(Boost_FOUND 1)
set(Boost_INCLUDE_DIRS "/home/polipo/devel/boost/boost_1_56_0")
set(Boost_LIBRARY_DIRS "/home/polipo/devel/boost/boost_1_56_0/first-build-mingw64/lib")

set (Boost_chrono_FOUND    1)
set (Boost_filesystem_FOUND  1)
set (Boost_iostreams_FOUND   1)
set (Boost_program_options_FOUND  1)
set (Boost_serialization_FOUND 1)
set (Boost_system_FOUND  1)
set (Boost_thread_FOUND  1)

set (Boost_chrono_LIBRARY    "/home/polipo/devel/boost/boost_1_56_0/first-build-mingw64/lib/libboost_chrono.dll")
set (Boost_filesystem_LIBRARY    "/home/polipo/devel/boost/boost_1_56_0/first-build-mingw64/lib/libboost_filesystem.dll")
set (Boost_iostreams_LIBRARY    "/home/polipo/devel/boost/boost_1_56_0/first-build-mingw64/lib/libboost_iostreams.dll")
set (Boost_program_options_LIBRARY    "/home/polipo/devel/boost/boost_1_56_0/first-build-mingw64/lib/libboost_program_options.dll")
set (Boost_serialization_LIBRARY    "/home/polipo/devel/boost/boost_1_56_0/first-build-mingw64/lib/libboost_serialization.dll")
set (Boost_system_LIBRARY    "/home/polipo/devel/boost/boost_1_56_0/first-build-mingw64/lib/libboost_system.dll")
set (Boost_thread_LIBRARY    "/home/polipo/devel/boost/boost_1_56_0/first-build-mingw64/lib/libboost_thread.dll")

set (Boost_VERSION   "1.56.0")
set (Boost_LIB_VERSION   "1_56")
set (Boost_MAJOR_VERSION  "1")
set (Boost_MINOR_VERSION   "56")
set (Boost_SUBMINOR_VERSION "0")

# remove console
set(CMAKE_EXE_LINKER_FLAGS
    "${CMAKE_EXE_LINKER_FLAGS} -Wl,--subsystem,windows"
)
