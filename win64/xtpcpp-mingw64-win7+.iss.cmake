[Setup]
AppName=X!TandemPipeline

; Set version number below
#define public version "${XTPCPP_VERSION}"
AppVersion={#version}

#define public arch "mingw64"
#define public platform "win7+"
#define sourceDir "C:\msys64\home\polipo\devel\xtandempipeline"
#define cmake_build_dir "C:\msys64\home\polipo\devel\xtandempipeline\build"

; Set version number below
AppVerName=X!TandemPipeline version {#version}
DefaultDirName={pf}\xtandempipeline
DefaultGroupName=xtandempipeline
OutputDir="C:\msys64\home\polipo\devel\xtandempipeline\win64"

; Set version number below
OutputBaseFilename=xtpcpp-{#arch}-{#platform}-v{#version}-setup

; Set version number below
OutputManifestFile=xtpcpp-{#arch}-{#platform}-v{#version}-setup-manifest.txt
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64

LicenseFile="{#sourceDir}\COPYING"
AppCopyright="Copyright (C) 2016- Olivier Langella"

AllowNoIcons=yes
AlwaysShowComponentsList=yes
AllowRootDirectory=no
AllowCancelDuringInstall=yes
AppComments="X!TandemPipeline, by Olivier Langella"
AppContact="Olivier Langella, research engineer at CNRS, France"
CloseApplications=yes
CreateUninstallRegKey=yes
DirExistsWarning=yes
WindowResizable=yes
; WizardImageFile="{#sourceDir}\images\splashscreen-innosetup.bmp"
WizardImageStretch=yes

[Dirs]
Name: "{app}"

[Files]
Source: "C:\xtpcpp-libdeps\*"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "C:\msys64\home\polipo\devel\libodsstream\build\src\libodsstream-qt5.so"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "C:\msys64\home\polipo\devel\pappsomspp\build\src\libpappsomspp-qt5.so"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "C:\msys64\home\polipo\devel\pappsomspp\build\src\pappsomspp\widget\libpappsomspp-widget-qt5.so"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

Source: "{#sourceDir}\README"; DestDir: {app}; Flags: isreadme; Components: xtpcppComp
Source: "{#sourceDir}\COPYING"; DestDir: {app}; Flags: isreadme; Components: xtpcppComp
Source: "{#sourceDir}\win64\xtandempipeline_icon.ico"; DestDir: {app}; Components: xtpcppComp

Source: "{#cmake_build_dir}\src\xtpcpp.exe"; DestDir: {app}; Components: xtpcppComp 

[Icons]
Name: "{group}\xtpcpp"; Filename: "{app}\xtpcpp.exe"; WorkingDir: "{app}";IconFilename: "{app}\xtandempipeline_icon.ico"
Name: "{group}\Uninstall xtpcpp"; Filename: "{uninstallexe}"

[Types]
Name: "xtpcppType"; Description: "Full installation"

[Components]
Name: "xtpcppComp"; Description: "X!TandemPipeline files"; Types: xtpcppType

[Run]
;Filename: "{app}\README"; Description: "View the README file"; Flags: postinstall shellexec skipifsilent
Filename: "{app}\xtpcpp.exe"; Description: "Launch X!TandemPipeline"; Flags: postinstall nowait unchecked
