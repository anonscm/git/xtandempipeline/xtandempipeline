# Bug report

## Version of X!TandemPipeline 
(Which version of X!TandemPipeline bug)

## Error message
(If an error message or a warning pop-up past-it here)

## File used
(Copy the file path used when the bug occured)

## Steps to reproduce
(How one can reproduce the issue - **very important**)

## What is the expected correct behavior?
(What you should see instead)

## Optional
### Relevant logs and/or screenshots
(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)

### Possible fixes
(If you can, link to the line of code that might be responsible for the problem)
